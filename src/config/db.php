<?php
    /**
     * Created by PhpStorm.
     * User: Klaas
     * Date: 21.11.2018
     * Time: 14:03
     */
    class db
    {
        private $host = 'localhost';
        private $db_name = 'fussball_liveticker';
        private $username = 'root';
        private $password = '';

        public function connect(){
            $mysql_connect_str = "mysql:host=$this->host;dbname=$this->db_name;charset=utf8";
            $dbConnection = new PDO($mysql_connect_str, $this->username, $this->password);
            $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return $dbConnection;
        }
    }