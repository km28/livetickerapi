<?php
    /**
     * Created by PhpStorm.
     * User: Klaas
     * Date: 07.01.2019
     * Time: 15:50
     */

    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;

    $app->get('/tickerereignisse/{id}', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $sql_query = "SELECT * FROM tickerereignisse WHERE spieldetails_id = $id ORDER BY tickerereignis_id DESC";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql_query);
            $tickerereignisse = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            echo json_encode($tickerereignisse);


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });


    $app->put('/tickerereignisse/hinzufuegen', function (Request $request, Response $response) {

        $tickerereignis_id = $request->getParam('tickerereignis_id');
        $spieldetails_id = $request->getParam('spieldetails_id');
        $spielminute = $request->getParam('spielminute');
        $ereignis = $request->getParam('ereignis');


        $sql_query = "INSERT INTO tickerereignisse (tickerereignis_id ,spieldetails_id, spielminute, ereignis) VALUES (:tickerereignis_id, :spieldetails_id, :spielminute, :ereignis)";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':tickerereignis_id', $tickerereignis_id);
            $stmt->bindParam(':spieldetails_id', $spieldetails_id);
            $stmt->bindParam(':spielminute', $spielminute);
            $stmt->bindParam(':ereignis', $ereignis);


            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "Tickerereignis hinzugefügt"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });
