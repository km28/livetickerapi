<?php
    /**
     * Created by PhpStorm.
     * User: Klaas
     * Date: 21.11.2018
     * Time: 15:44
     */

    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;


    $app->get('/spielorte', function(Request $request, Response $response){

        $sql_query = "SELECT * FROM spielorte";
        try{
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql_query);
            $spielorte = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            echo json_encode($spielorte);


        }catch (PDOException $e){
            echo '{"error"; {"text": ' .$e->getMessage().'}';
        }
    });

    $app->get('/spielorte/{platzname}', function(Request $request, Response $response){

        $platzname = '"'.$request->getAttribute('platzname').'"';
        $sql_query = "SELECT * FROM spielorte WHERE platzname = $platzname";
        try{
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql_query);
            $spielort = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            echo json_encode($spielort);


        }catch (PDOException $e){
            echo '{"error"; {"text": ' .$e->getMessage().'}';
        }
    });