<?php
    /**
     * Created by PhpStorm.
     * User: Klaas
     * Date: 22.11.2018
     * Time: 14:24
     */


    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;

    $app->get('/spieldetails', function (Request $request, Response $response) {

        $sql_query = "SELECT * FROM spieldetails";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql_query);
            $spieldetails = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            echo json_encode($spieldetails);


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->get('/spieldetails/{id}', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');

        $sql_query = "SELECT * FROM spieldetails WHERE spieldetails_id = $id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql_query);
            $spieldetails = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            echo json_encode($spieldetails);


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });


    $app->put('/spieldetails/hinzufuegen', function (Request $request, Response $response) {

        $id = $request->getParam('spieldetails_id');
        $verfasser = $request->getParam('verfasser');
        $tore_heim = $request->getParam('tore_heim');
        $tore_ausw = $request->getParam('tore_ausw');
        $ecken_heim = $request->getParam('ecken_heim');
        $ecken_ausw = $request->getParam('ecken_ausw');
        $gkarte_heim = $request->getParam('gkarten_heim');
        $gkarte_ausw = $request->getParam('gkarten_ausw');
        $rkarte_heim = $request->getParam('rkarten_heim');
        $rkarte_ausw = $request->getParam('rkarten_ausw');
        $ballbesitz_heim = $request->getParam('ballbesitz_heim');
        $ballbesitz_ausw = $request->getParam('ballbesitz_ausw');
        $torschuesse_heim = $request->getParam('torschuesse_heim');
        $torschuesse_ausw = $request->getParam('torschuesse_ausw');
        $fouls_heim = $request->getParam('fouls_heim');
        $fouls_ausw = $request->getParam('fouls_ausw');
        $abseits_heim = $request->getParam('abseits_heim');
        $abseits_ausw = $request->getParam('abseits_ausw');


        $sql_query = "INSERT INTO spieldetails (spieldetails_id, verfasser, tore_heim, tore_ausw, ecken_heim, ecken_ausw, gkarten_heim, gkarten_ausw,
                      rkarten_heim, rkarten_ausw, ballbesitz_heim, ballbesitz_ausw, torschuesse_heim, torschuesse_ausw, fouls_heim, fouls_ausw,
                       abseits_heim, abseits_ausw) VALUES (:spieldetails_id, :verfasser, :tore_heim, :tore_ausw, :ecken_heim, :ecken_ausw, :gkarten_heim, :gkarten_ausw,
                      :rkarten_heim, :rkarten_ausw, :ballbesitz_heim, :ballbesitz_ausw, :torschuesse_heim, :torschuesse_ausw, :fouls_heim, :fouls_ausw,
                       :abseits_heim, :abseits_ausw)";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':verfasser', $verfasser);
            $stmt->bindParam(':tore_heim', $tore_heim);
            $stmt->bindParam(':tore_ausw', $tore_ausw);
            $stmt->bindParam(':ecken_heim', $ecken_heim);
            $stmt->bindParam(':ecken_ausw', $ecken_ausw);
            $stmt->bindParam(':gkarten_heim', $gkarte_heim);
            $stmt->bindParam(':gkarten_ausw', $gkarte_ausw);
            $stmt->bindParam(':rkarten_heim', $rkarte_heim);
            $stmt->bindParam(':rkarten_ausw', $rkarte_ausw);
            $stmt->bindParam(':ballbesitz_heim', $ballbesitz_heim);
            $stmt->bindParam(':ballbesitz_ausw', $ballbesitz_ausw);
            $stmt->bindParam(':torschuesse_heim', $torschuesse_heim);
            $stmt->bindParam(':torschuesse_ausw', $torschuesse_ausw);
            $stmt->bindParam(':fouls_heim', $fouls_heim);
            $stmt->bindParam(':fouls_ausw', $fouls_ausw);
            $stmt->bindParam(':abseits_heim', $abseits_heim);
            $stmt->bindParam(':abseits_ausw', $abseits_ausw);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "Spieldetails hinzugefügt"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spieldetails/{id}/update_tore_heim', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $tore_heim = $request->getParam('tore_heim');


        $sql_query = "UPDATE spieldetails SET tore_heim = :tore_heim WHERE spieldetails_id = :spieldetails_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':tore_heim', $tore_heim);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "tore_heim updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spieldetails/{id}/update_tore_ausw', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $tore_ausw = $request->getParam('tore_ausw');


        $sql_query = "UPDATE spieldetails SET tore_ausw = :tore_ausw WHERE spieldetails_id = :spieldetails_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':tore_ausw', $tore_ausw);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "tore_ausw updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spieldetails/{id}/update_ecken_heim', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $ecken_heim = $request->getParam('ecken_heim');


        $sql_query = "UPDATE spieldetails SET ecken_heim = :ecken_heim WHERE spieldetails_id = :spieldetails_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':ecken_heim', $ecken_heim);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "ecken_heim updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spieldetails/{id}/update_ecken_ausw', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $ecken_ausw = $request->getParam('ecken_ausw');


        $sql_query = "UPDATE spieldetails SET ecken_ausw = :ecken_ausw WHERE spieldetails_id = :spieldetails_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':ecken_ausw', $ecken_ausw);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "ecken_ausw updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spieldetails/{id}/update_gkarten_heim', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $gkarten_heim = $request->getParam('gkarten_heim');


        $sql_query = "UPDATE spieldetails SET gkarten_heim = :gkarten_heim WHERE spieldetails_id = :spieldetails_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':gkarten_heim', $gkarten_heim);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "gkarten_heim updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spieldetails/{id}/update_gkarten_ausw', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $gkarten_ausw = $request->getParam('gkarten_ausw');


        $sql_query = "UPDATE spieldetails SET gkarten_ausw = :gkarten_ausw WHERE spieldetails_id = :spieldetails_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':gkarten_ausw', $gkarten_ausw);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "gkarten_ausw updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spieldetails/{id}/update_rkarten_heim', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $rkarten_heim = $request->getParam('rkarten_heim');


        $sql_query = "UPDATE spieldetails SET rkarten_heim = :rkarten_heim WHERE spieldetails_id = :spieldetails_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':rkarten_heim', $rkarten_heim);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "rkarten_heim updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spieldetails/{id}/update_rkarten_ausw', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $rkarten_ausw = $request->getParam('rkarten_ausw');


        $sql_query = "UPDATE spieldetails SET rkarten_ausw = :rkarten_ausw WHERE spieldetails_id = :spieldetails_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':rkarten_ausw', $rkarten_ausw);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "rkarten_ausw updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spieldetails/{id}/update_ballbesitz_heim', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $ballbesitz_heim = $request->getParam('ballbesitz_heim');


        $sql_query = "UPDATE spieldetails SET ballbesitz_heim = :ballbesitz_heim WHERE spieldetails_id = :spieldetails_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':ballbesitz_heim', $ballbesitz_heim);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "ballbesitz_heim updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spieldetails/{id}/update_ballbesitz_ausw', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $ballbesitz_ausw = $request->getParam('ballbesitz_ausw');


        $sql_query = "UPDATE spieldetails SET ballbesitz_ausw = :ballbesitz_ausw WHERE spieldetails_id = :spieldetails_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':ballbesitz_ausw', $ballbesitz_ausw);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "ballbesitz_ausw updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spieldetails/{id}/update_torschuesse_heim', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $torschuesse_heim = $request->getParam('torschuesse_heim');


        $sql_query = "UPDATE spieldetails SET torschuesse_heim = :torschuesse_heim WHERE spieldetails_id = :spieldetails_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':torschuesse_heim', $torschuesse_heim);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "torschuesse_heim updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spieldetails/{id}/update_torschuesse_ausw', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $torschuesse_ausw = $request->getParam('torschuesse_ausw');


        $sql_query = "UPDATE spieldetails SET torschuesse_ausw = :torschuesse_ausw WHERE spieldetails_id = :spieldetails_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':torschuesse_ausw', $torschuesse_ausw);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "torschuesse_ausw updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spieldetails/{id}/update_fouls_heim', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $fouls_heim = $request->getParam('fouls_heim');


        $sql_query = "UPDATE spieldetails SET fouls_heim = :fouls_heim WHERE spieldetails_id = :spieldetails_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':fouls_heim', $fouls_heim);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "fouls_heim updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spieldetails/{id}/update_fouls_ausw', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $fouls_ausw = $request->getParam('fouls_ausw');


        $sql_query = "UPDATE spieldetails SET fouls_ausw = :fouls_ausw WHERE spieldetails_id = :spieldetails_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':fouls_ausw', $fouls_ausw);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "fouls_ausw updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spieldetails/{id}/update_abseits_heim', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $abseits_heim = $request->getParam('abseits_heim');


        $sql_query = "UPDATE spieldetails SET abseits_heim = :abseits_heim WHERE spieldetails_id = :spieldetails_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':abseits_heim', $abseits_heim);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "abseits_heim updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spieldetails/{id}/update_abseits_ausw', function (Request $request, Response $response) {

        $id = $request->getAttribute('id');
        $abseits_ausw = $request->getParam('abseits_ausw');


        $sql_query = "UPDATE spieldetails SET abseits_ausw = :abseits_ausw WHERE spieldetails_id = :spieldetails_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spieldetails_id', $id);
            $stmt->bindParam(':abseits_ausw', $abseits_ausw);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "abseits_ausw updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });





