<?php
    /**
     * Created by PhpStorm.
     * User: Klaas
     * Date: 21.11.2018
     * Time: 13:59
     */

    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;


    $app->get('/spiele', function(Request $request, Response $response){

       $sql_request = "SELECT * FROM spiele";
       try{
           $db = new db();
           $db = $db->connect();
           $stmt = $db->query($sql_request);
           $spiele = $stmt->fetchAll(PDO::FETCH_OBJ);
           $db = null;
           echo json_encode($spiele);


       }catch (PDOException $e){
           echo '{"error"; {"text": ' .$e->getMessage().'}';
       }
    });


    $app->get('/spiele/{liga}/{spieltag}', function(Request $request, Response $response){

        $liga = '"'.$request->getAttribute('liga').'"';
        $spieltag = $request->getAttribute('spieltag');
        $sql_query = "SELECT * FROM spiele WHERE liga = $liga AND spieltag = $spieltag";
        try{
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql_query);
            $spieltagsspiele = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            echo json_encode($spieltagsspiele);


        }catch (PDOException $e){
            echo '{"error"; {"text": ' .$e->getMessage().'}';
        }
    });

    $app->get('/spiele/{liga}', function(Request $request, Response $response){

        $liga = '"'.$request->getAttribute('liga').'"';
        $sql_query = "SELECT * FROM spiele WHERE liga = $liga";
        try{
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql_query);
            $spieltagsspiele = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            echo json_encode($spieltagsspiele);


        }catch (PDOException $e){
            echo '{"error"; {"text": ' .$e->getMessage().'}';
        }
    });

    $app->patch('/spiele/{id}/update_spielvorbei', function(Request $request, Response $response){
        $id = $request->getAttribute('id');
        $spielvorbei = $request->getParam('spielvorbei');


        $sql_query = "UPDATE spiele SET spielvorbei = :spielvorbei WHERE spiel_id = :spiel_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spiel_id', $id);
            $stmt->bindParam(':spielvorbei', $spielvorbei);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "spielvorbei updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spiele/{id}/update_halbzeitergebnis', function(Request $request, Response $response){
        $id = $request->getAttribute('id');
        $halbzeitergebnis = $request->getParam('halbzeitergebnis');


        $sql_query = "UPDATE spiele SET halbzeitergebnis = :halbzeitergebnis WHERE spiel_id = :spiel_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spiel_id', $id);
            $stmt->bindParam(':halbzeitergebnis', $halbzeitergebnis);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "halbzeitergebnis updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spiele/{id}/update_endergebnis', function(Request $request, Response $response){
        $id = $request->getAttribute('id');
        $endergebnis = $request->getParam('endergebnis');


        $sql_query = "UPDATE spiele SET endergebnis = :endergebnis WHERE spiel_id = :spiel_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spiel_id', $id);
            $stmt->bindParam(':endergebnis', $endergebnis);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "endergebnis updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->patch('/spiele/{id}/update_spieldetails', function(Request $request, Response $response){
        $id = $request->getAttribute('id');
        $spieldetails = $request->getParam('spieldetails');


        $sql_query = "UPDATE spiele SET spieldetails = :spieldetails WHERE spiel_id = :spiel_id";
        try {
            $db = new db();
            $db = $db->connect();

            $stmt = $db->prepare($sql_query);

            $stmt->bindParam(':spiel_id', $id);
            $stmt->bindParam(':spieldetails', $spieldetails);

            $stmt->execute();
            $db = null;
            echo '{"notice": {"text": "spieldetails updated"}}';


        } catch (PDOException $e) {
            echo '{"error"; {"text": ' . $e->getMessage() . '}';
        }
    });

    $app->get('/spiele/{spiel_id}/', function(Request $request, Response $response){

        $id = $request->getAttribute('spiel_id');
        $sql_query = "SELECT * FROM spiele WHERE spiel_id = $id";
        try{
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql_query);
            $spiel = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            echo json_encode($spiel);


        }catch (PDOException $e){
            echo '{"error"; {"text": ' .$e->getMessage().'}';
        }
    });
