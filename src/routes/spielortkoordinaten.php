<?php
    /**
     * Created by PhpStorm.
     * User: Klaas
     * Date: 21.11.2018
     * Time: 15:51
     */

    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;

    $app = new \Slim\App;

    $app->get('/spielortekoordinaten', function(Request $request, Response $response){

        $sql_request = "SELECT * FROM spielortkoordinaten";
        try{
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql_request);
            $spielortekoordinaten = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            echo json_encode($spielortekoordinaten);


        }catch (PDOException $e){
            echo '{"error"; {"text": ' .$e->getMessage().'}';
        }
    });


    $app->get('/spielortekoordinaten/platzname', function(Request $request, Response $response){

        $platzname = '"'.$request->getAttribute('platzname').'"';
        $sql_request = "SELECT * FROM spielortkoordinaten WHERE platzname = $platzname";
        try{
            $db = new db();
            $db = $db->connect();

            $stmt = $db->query($sql_request);
            $spieltagsspiele = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            echo json_encode($spieltagsspiele);


        }catch (PDOException $e){
            echo '{"error"; {"text": ' .$e->getMessage().'}';
        }
    });