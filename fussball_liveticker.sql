-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 05. Feb 2019 um 06:41
-- Server-Version: 10.1.36-MariaDB
-- PHP-Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `fussball_liveticker`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mannschaften`
--

CREATE TABLE `mannschaften` (
  `mannschaftsname` varchar(45) CHARACTER SET utf8 NOT NULL,
  `fupa_kader` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `mannschaften`
--

INSERT INTO `mannschaften` (`mannschaftsname`, `fupa_kader`) VALUES
('1. FC Schinkel', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9dcbc3385\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(599957,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9dcbc3385\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/1-fc-schinkel\" target=\"_blank\" class=\"fupa_link\">1. FC Schinkel auf FuPa</a>'),
('ASV Dersau', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9d913e561\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(611021,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9d913e561\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/asv-dersau\" target=\"_blank\" class=\"fupa_link\">ASV Dersau auf FuPa</a>'),
('Bujendorfer Spvg', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9eb5a5e1a\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(610954,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9eb5a5e1a\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/bujendorfer-spvg\" target=\"_blank\" class=\"fupa_link\">Bujendorfer SpVg auf FuPa</a>'),
('Dobersdorfer SV', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9d165f70a\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(599955,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9d165f70a\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/dobersdorfer-sv\" target=\"_blank\" class=\"fupa_link\">Dobersdorfer SV auf FuPa</a>'),
('Eutin 08 II', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9dec33b72\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(599950,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9dec33b72\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/eutin-08\" target=\"_blank\" class=\"fupa_link\">Eutin 08 II auf FuPa</a>'),
('FC Kilia Kiel', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9e7353328\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(584833,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9e7353328\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/fc-kilia-kiel\" target=\"_blank\" class=\"fupa_link\">FC Kilia Kiel auf FuPa</a>'),
('MTV Dänischenhagen', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9c5c9f635\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(599946,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9c5c9f635\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/mtv-daenischenhagen\" target=\"_blank\" class=\"fupa_link\">MTV Dänischenhagen auf FuPa</a>'),
('Preetzer TSV', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9dfea9f71\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(599943,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9dfea9f71\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/preetzer-tsv\" target=\"_blank\" class=\"fupa_link\">Preetzer TSV auf FuPa</a>'),
('SC Cismar', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9ee26c639\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(610974,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9ee26c639\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/sc-cismar\" target=\"_blank\" class=\"fupa_link\">SC Cismar auf FuPa</a>'),
('SG Baltic 1', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9ff871b2e\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(610949,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9ff871b2e\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/tsv-dahme\" target=\"_blank\" class=\"fupa_link\">SG Baltic auf FuPa</a>'),
('SG Bornhöved/Schmalensee', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9ca5cf2f2\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(610856,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9ca5cf2f2\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/tsv-quellenhaupt-bornhoeved\" target=\"_blank\" class=\"fupa_link\">SG Bornhöved / Schmalensee auf FuPa</a>'),
('SG Insel Fehmarn I', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9e25ea61b\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(599952,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9e25ea61b\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/rsv-landkirchen\" target=\"_blank\" class=\"fupa_link\">SG Insel Fehmarn / Landkirchen auf FuPa</a>'),
('SG Insel Fehmarn II', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9f8c579f5\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(610946,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9f8c579f5\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/rsv-landkirchen\" target=\"_blank\" class=\"fupa_link\">SG Insel Fehmarn II auf FuPa</a>'),
('SG Lensahn/Kabelhorst I', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9f6e270c3\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(610948,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9f6e270c3\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/kabelhorst-schwienkuhler-sc\" target=\"_blank\" class=\"fupa_link\">SG Lensahn/Kabelhors auf FuPa</a>'),
('SG Scharbeutz/Süsel', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9f22dc738\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(610825,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9f22dc738\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/fc-scharbeutz\" target=\"_blank\" class=\"fupa_link\">SG Scharbeutz/Süsel auf FuPa</a>'),
('SSG Rot-Schwarz Kiel', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9d313c459\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(599947,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9d313c459\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/ssg-rot-schwarz-kiel\" target=\"_blank\" class=\"fupa_link\">SSG Rot-Schwarz Kiel auf FuPa</a>'),
('SV Großenbrode', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9f99c23bd\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(610953,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9f99c23bd\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/sv-grossenbrode\" target=\"_blank\" class=\"fupa_link\">SV Großenbrode auf FuPa</a>'),
('SV Heringsdorf', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9eedb437e\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(610978,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9eedb437e\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/sv-heringsdorf\" target=\"_blank\" class=\"fupa_link\">SV Heringsdorf auf FuPa</a>'),
('SVE Comet Kiel', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9bf4391a5\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(599948,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9bf4391a5\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/sve-comet-kiel\" target=\"_blank\" class=\"fupa_link\">SVE Comet Kiel auf FuPa</a>'),
('TSV Kronshagen', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9c305c652\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(599944,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9c305c652\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/tsv-kronshagen\" target=\"_blank\" class=\"fupa_link\">TSV Kronshagen auf FuPa</a>'),
('TSV Lepahn', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9fea522ae\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(611039,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9fea522ae\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/tsv-lepahn\" target=\"_blank\" class=\"fupa_link\">TSV Lepahn auf FuPa</a>'),
('TSV Lütjenburg II', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9fbd1b35f\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(610955,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9fbd1b35f\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/tsv-luetjenburg\" target=\"_blank\" class=\"fupa_link\">TSV Lütjenburg II auf FuPa</a>'),
('TSV Malente', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9e401bef7\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(599951,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9e401bef7\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/tsv-malente\" target=\"_blank\" class=\"fupa_link\">TSV Malente auf FuPa</a>'),
('TSV Malente II', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9fcc74d1f\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(610945,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9fcc74d1f\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/tsv-malente\" target=\"_blank\" class=\"fupa_link\">TSV Malente II auf FuPa</a>'),
('TSV Plön', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9cf481033\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(599953,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9cf481033\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/tsv-ploen\" target=\"_blank\" class=\"fupa_link\">TSV Plön auf FuPa</a>'),
('TSV Schönwalde', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9f0ebfea7\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(610944,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9f0ebfea7\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/tsv-schoenwalde-am-bungsberg\" target=\"_blank\" class=\"fupa_link\">TSV Schönwalde am Bungsberg auf FuPa</a>'),
('TSV Selent', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9ec7ee9e4\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(611024,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9ec7ee9e4\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/tsv-selent\" target=\"_blank\" class=\"fupa_link\">TSV Selent auf FuPa</a>'),
('TSV Stein', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9da94e5cf\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(599945,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9da94e5cf\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/tsv-stein\" target=\"_blank\" class=\"fupa_link\">TSV Stein auf FuPa</a>'),
('TSV Wentorf', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9f51f01ec\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(599956,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9f51f01ec\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/tsv-wentorf\" target=\"_blank\" class=\"fupa_link\">TSV Wentorf auf FuPa</a>'),
('VfR Laboe', '<script type=\"text/javascript\" src=\"https://www.fupa.net/fupa/widget.min.js\"></script>\r\n<div id=\"widget_5bed9cd76a1b5\" >... lade FuPa Widget ...\r\n<script type=\"text/javascript\">\r\n!function(i){window.setTimeout(function(){\"undefined\"===typeof fupa_widget_domain?--i.t>0&&window.setTimeout(arguments.callee,i.i):i.f()},i.i)}({i:20,t:100,f:function(){team_widget(610610,{act:\"team\",hea:0,nav:0,div:\"widget_5bed9cd76a1b5\",mod:\"0\"})}});</script>\r\n</div><a href=\"https://www.fupa.net/club/vfr-laboe\" target=\"_blank\" class=\"fupa_link\">VfR Laboe auf FuPa</a>');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `spieldetails`
--

CREATE TABLE `spieldetails` (
  `spieldetails_id` int(11) NOT NULL,
  `verfasser` varchar(45) NOT NULL,
  `tore_heim` int(11) DEFAULT NULL,
  `tore_ausw` int(11) DEFAULT NULL,
  `ecken_heim` int(11) DEFAULT NULL,
  `ecken_ausw` int(11) DEFAULT NULL,
  `gkarten_heim` int(11) DEFAULT NULL,
  `gkarten_ausw` int(11) DEFAULT NULL,
  `rkarten_heim` int(11) DEFAULT NULL,
  `rkarten_ausw` int(11) DEFAULT NULL,
  `ballbesitz_heim` int(11) DEFAULT NULL,
  `ballbesitz_ausw` int(11) DEFAULT NULL,
  `torschuesse_heim` int(11) DEFAULT NULL,
  `torschuesse_ausw` int(11) DEFAULT NULL,
  `fouls_heim` int(11) DEFAULT NULL,
  `fouls_ausw` int(11) DEFAULT NULL,
  `abseits_heim` int(11) DEFAULT NULL,
  `abseits_ausw` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `spieldetails`
--

INSERT INTO `spieldetails` (`spieldetails_id`, `verfasser`, `tore_heim`, `tore_ausw`, `ecken_heim`, `ecken_ausw`, `gkarten_heim`, `gkarten_ausw`, `rkarten_heim`, `rkarten_ausw`, `ballbesitz_heim`, `ballbesitz_ausw`, `torschuesse_heim`, `torschuesse_ausw`, `fouls_heim`, `fouls_ausw`, `abseits_heim`, `abseits_ausw`) VALUES
(1, 'Peter', 1, 2, 4, 6, 2, 1, 0, 0, 50, 50, 6, 4, 8, 6, 3, 2),
(241, 'Klaus', 4, 1, 9, 2, 3, 3, 0, 0, 50, 50, 11, 5, 8, 7, 2, 4),
(423, 'Klaas', 2, 2, 2, 2, 2, 2, 2, 2, 50, 50, 2, 2, 2, 2, 2, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `spiele`
--

CREATE TABLE `spiele` (
  `spiel_id` int(11) NOT NULL,
  `liga` varchar(45) NOT NULL,
  `spieltag` int(11) NOT NULL,
  `heimmannschaft` varchar(45) NOT NULL,
  `auswaertsmannschaft` varchar(45) NOT NULL,
  `anstosszeit` datetime NOT NULL,
  `spielvorbei` tinyint(1) NOT NULL,
  `spielort` varchar(45) NOT NULL,
  `endergebnis` varchar(10) DEFAULT NULL,
  `halbzeitergebnis` varchar(10) DEFAULT NULL,
  `spieldetails` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `spiele`
--

INSERT INTO `spiele` (`spiel_id`, `liga`, `spieltag`, `heimmannschaft`, `auswaertsmannschaft`, `anstosszeit`, `spielvorbei`, `spielort`, `endergebnis`, `halbzeitergebnis`, `spieldetails`) VALUES
(1, 'Verbandsliga Ost', 1, 'FC Kilia Kiel', 'TSV Kronshagen', '2018-08-04 15:00:00', 1, 'Hauptplatz', '1:2', '0:0', 1),
(2, 'Verbandsliga Ost', 1, 'VfR Laboe', 'SG Bornhöved/Schmalensee', '2018-08-04 15:00:00', 1, 'Stoschplatz Hauptplatz', '3:0', '1:0', NULL),
(3, 'Verbandsliga Ost', 1, '1. FC Schinkel', 'TSV Plön', '2018-08-04 15:00:00', 1, 'A-Platz Schinkel', '2:4', '1:2', NULL),
(4, 'Verbandsliga Ost', 1, 'SSG Rot-Schwarz Kiel', 'TSV Stein', '2018-08-04 16:00:00', 1, 'Sportplatz Meimersdorf', '1:2', '1:1', NULL),
(5, 'Verbandsliga Ost', 1, 'Dobersdorfer SV', 'ASV Dersau', '2018-08-05 15:00:00', 1, 'Sportplatz Dieter von Bortstel	', '1:2', '1:1', NULL),
(6, 'Verbandsliga Ost', 1, 'Eutin 08 II', 'SVE Comet Kiel', '2018-08-05 15:00:00', 1, 'Fritz-Latendorf-Stadion ', '1:5', '0:4', NULL),
(7, 'Verbandsliga Ost', 1, 'Preetzer TSV', 'SG Insel Fehmarn I', '2018-08-05 15:00:00', 1, 'Stadion Preetz', '1:0', '1:0', NULL),
(8, 'Verbandsliga Ost', 1, 'TSV Malente', 'MTV Dänischenhagen', '2018-08-05 15:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '0:2', '0:2', NULL),
(9, 'Verbandsliga Ost', 2, 'TSV Kronshagen', '1. FC Schinkel', '2018-08-08 19:00:00', 1, 'Sportzentrum Platz4', '1:2', '0:1', NULL),
(10, 'Verbandsliga Ost', 2, 'TSV Plön', 'TSV Malente', '2018-08-08 19:00:00', 1, 'Schiffsthal Stadion', '2:1', '0:1', NULL),
(11, 'Verbandsliga Ost', 2, 'MTV Dänischenhagen', 'SSG Rot-Schwarz Kiel', '2018-08-08 19:00:00', 1, 'Sportanlage Dänischenhagen	', '1:1', '0:1', NULL),
(12, 'Verbandsliga Ost', 2, 'TSV Stein', 'Preetzer TSV', '2018-08-08 19:00:00', 1, 'Sportplatz Stein', '5:2', '3:1', NULL),
(13, 'Verbandsliga Ost', 2, 'SVE Comet Kiel', 'Dobersdorfer SV', '2018-08-08 19:00:00', 1, 'A-Platz SVE Comet Kiel', '2:0', '1:0', NULL),
(14, 'Verbandsliga Ost', 2, 'ASV Dersau', 'VfR Laboe', '2018-08-08 19:00:00', 1, 'A-Platz Dersau', '2:2', '1:0', NULL),
(15, 'Verbandsliga Ost', 2, 'SG Bornhöved/Schmalensee', 'FC Kilia Kiel', '2018-08-08 19:00:00', 1, 'Seeweg Bornhoeved A-Platz', '2:5', '2:2', NULL),
(16, 'Verbandsliga Ost', 2, 'SG Insel Fehmarn I', 'Eutin 08 II', '2018-08-15 19:00:00', 1, 'RSV1', '1:2', '0:1', NULL),
(17, 'Verbandsliga Ost', 3, 'TSV Plön', 'Preetzer TSV', '2018-08-10 19:00:00', 1, 'Schiffsthal Stadion', '3:0', '2:0', NULL),
(18, 'Verbandsliga Ost', 3, 'TSV Kronshagen', 'SSG Rot-Schwarz Kiel', '2018-08-11 14:00:00', 1, 'Sportzentrum Platz4', '3:2', '2:1', NULL),
(19, 'Verbandsliga Ost', 3, 'SVE Comet Kiel', 'SG Bornhöved/Schmalensee', '2018-08-11 15:00:00', 1, 'A-Platz SVE Comet Kiel', '5:1', '1:0', NULL),
(20, 'Verbandsliga Ost', 3, 'ASV Dersau', 'FC Kilia Kiel', '2018-08-11 15:00:00', 1, 'A-Platz Dersau', '3:0', '2:0', NULL),
(21, 'Verbandsliga Ost', 3, '1. FC Schinkel', 'TSV Malente', '2018-08-11 15:00:00', 1, 'A-Platz Schinkel', '4:2', '0:1', NULL),
(22, 'Verbandsliga Ost', 3, 'MTV Dänischenhagen', 'Eutin 08 II', '2018-08-11 15:30:00', 1, 'Sportanlage Dänischenhagen	', '2:0', '0:0', NULL),
(23, 'Verbandsliga Ost', 3, 'Dobersdorfer SV', 'TSV Stein', '2018-08-11 16:00:00', 1, 'Sportplatz Dieter von Bortstel	', '1:0', '0:0', NULL),
(24, 'Verbandsliga Ost', 3, 'SG Insel Fehmarn I', 'VfR Laboe', '2018-08-12 16:00:00', 1, 'RSV1', '1:7', '1:6', NULL),
(25, 'Verbandsliga Ost', 4, 'ASV Dersau', 'SG Bornhöved/Schmalensee', '2018-08-17 18:30:00', 1, 'A-Platz Dersau', '2:1', '0:1', NULL),
(26, 'Verbandsliga Ost', 4, 'TSV Plön', 'SSG Rot-Schwarz Kiel', '2018-08-17 19:45:00', 1, 'Schiffsthal Stadion', '4:0', '1:0', NULL),
(27, 'Verbandsliga Ost', 4, 'FC Kilia Kiel', '1. FC Schinkel', '2018-08-18 15:00:00', 1, 'Hauptplatz', '4:1', '2:0', NULL),
(28, 'Verbandsliga Ost', 4, 'VfR Laboe', 'SVE Comet Kiel', '2018-08-18 15:00:00', 1, 'Stoschplatz Hauptplatz', '2:1', '0:0', NULL),
(29, 'Verbandsliga Ost', 4, 'Preetzer TSV', 'MTV Dänischenhagen', '2018-08-18 15:30:00', 1, 'Stadion Preetz', '1:3', '0:2', NULL),
(30, 'Verbandsliga Ost', 4, 'Dobersdorfer SV', 'SG Insel Fehmarn I', '2018-08-19 15:00:00', 1, 'Sportplatz Dieter von Bortstel	', '1:1', '1:0', NULL),
(31, 'Verbandsliga Ost', 4, 'Eutin 08 II', 'TSV Stein', '2018-08-19 15:00:00', 1, 'Fritz-Latendorf-Stadion ', '6:1', '2:1', NULL),
(32, 'Verbandsliga Ost', 4, 'TSV Malente', 'TSV Kronshagen', '2018-08-19 15:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '0:4', '0:3', NULL),
(33, 'Verbandsliga Ost', 5, 'VfR Laboe', 'TSV Stein', '2018-08-24 18:45:00', 1, 'Stoschplatz Hauptplatz', '0:0', '0:0', NULL),
(34, 'Verbandsliga Ost', 5, '1. FC Schinkel', 'SSG Rot-Schwarz Kiel', '2018-08-25 15:00:00', 1, 'A-Platz Schinkel', '1:9', '1:4', NULL),
(35, 'Verbandsliga Ost', 5, 'ASV Dersau', 'SVE Comet Kiel', '2018-08-25 15:00:00', 1, 'A-Platz Dersau', '1:2', '0:1', NULL),
(36, 'Verbandsliga Ost', 5, 'Preetzer TSV', 'TSV Kronshagen', '2018-08-25 15:30:00', 1, 'Stadion Preetz', '0:6', '0:4', NULL),
(37, 'Verbandsliga Ost', 5, 'TSV Malente', 'FC Kilia Kiel', '2018-08-26 15:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '0:0', '0:0', NULL),
(38, 'Verbandsliga Ost', 5, 'Dobersdorfer SV', 'MTV Dänischenhagen', '2018-08-26 15:00:00', 1, 'Sportplatz Dieter von Bortstel	', '0:4', '0:2', NULL),
(39, 'Verbandsliga Ost', 5, 'Eutin 08 II', 'TSV Plön', '2018-08-26 15:00:00', 1, 'Fritz-Latendorf-Stadion ', '1:3', '1:1', NULL),
(40, 'Verbandsliga Ost', 5, 'SG Bornhöved/Schmalensee', 'SG Insel Fehmarn I', '2018-08-26 15:00:00', 1, 'Seeweg Bornhoeved A-Platz', '2:1', '0:0', NULL),
(41, 'Verbandsliga Ost', 6, 'TSV Plön', 'Dobersdorfer SV', '2018-08-31 19:30:00', 1, 'Schiffsthal Stadion', '2:2', '1:0', NULL),
(42, 'Verbandsliga Ost', 6, 'TSV Kronshagen', 'Eutin 08 II', '2018-09-01 14:00:00', 1, 'Sportzentrum Platz4', '5:0', '5:0', NULL),
(43, 'Verbandsliga Ost', 6, 'TSV Stein', 'SG Bornhöved/Schmalensee', '2018-09-01 14:00:00', 1, 'Sportplatz Stein', '1:2', '0:2', NULL),
(44, 'Verbandsliga Ost', 6, 'SVE Comet Kiel', 'FC Kilia Kiel', '2018-09-01 15:00:00', 1, 'A-Platz SVE Comet Kiel', '9:1', '2:0', NULL),
(45, 'Verbandsliga Ost', 6, '1. FC Schinkel', 'Preetzer TSV', '2018-09-01 15:00:00', 1, 'A-Platz Schinkel', '2:1', '0:1', NULL),
(46, 'Verbandsliga Ost', 6, 'MTV Dänischenhagen', 'VfR Laboe', '2018-09-01 15:30:00', 1, 'Sportanlage Dänischenhagen	', '2:2', '1:1', NULL),
(47, 'Verbandsliga Ost', 6, 'SG Insel Fehmarn I', 'ASV Dersau', '2018-09-02 15:00:00', 1, 'RSV1', '2:2', '0:0', NULL),
(48, 'Verbandsliga Ost', 6, 'TSV Malente', 'SSG Rot-Schwarz Kiel', '2018-09-02 15:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '2:2', '1:1', NULL),
(49, 'Verbandsliga Ost', 7, 'FC Kilia Kiel', 'SSG Rot-Schwarz Kiel', '2018-09-08 15:00:00', 1, 'Hauptplatz', '2:10', '0:6', NULL),
(50, 'Verbandsliga Ost', 7, 'VfR Laboe', 'TSV Plön', '2018-09-08 15:00:00', 1, 'Stoschplatz Hauptplatz', '2:2', '0:0', NULL),
(51, 'Verbandsliga Ost', 7, 'SVE Comet Kiel', 'SG Insel Fehmarn I', '2018-09-08 15:00:00', 1, 'A-Platz SVE Comet Kiel', '4:0', '3:0', NULL),
(52, 'Verbandsliga Ost', 7, 'ASV Dersau', 'TSV Stein', '2018-09-08 15:00:00', 1, 'A-Platz Dersau', '2:0', '2:0', NULL),
(53, 'Verbandsliga Ost', 7, 'Dobersdorfer SV', 'TSV Kronshagen', '2018-09-09 15:00:00', 1, 'Sportplatz Dieter von Bortstel	', '2:7', '0:3', NULL),
(54, 'Verbandsliga Ost', 7, 'Eutin 08 II', '1. FC Schinkel', '2018-09-09 15:00:00', 1, 'Fritz-Latendorf-Stadion ', '3:0', '0:0', NULL),
(55, 'Verbandsliga Ost', 7, 'Preetzer TSV', 'TSV Malente', '2018-09-09 15:00:00', 1, 'Stadion Preetz', '2:1', '2:0', NULL),
(56, 'Verbandsliga Ost', 7, 'SG Bornhöved/Schmalensee', 'MTV Dänischenhagen', '2018-09-09 15:00:00', 1, 'Seeweg Bornhoeved A-Platz', '1:0', '1:0', NULL),
(57, 'Verbandsliga Ost', 8, 'TSV Plön', 'SG Bornhöved/Schmalensee', '2018-09-14 19:00:00', 1, 'Schiffsthal Stadion', '1:4', '1:2', NULL),
(58, 'Verbandsliga Ost', 8, 'TSV Kronshagen', 'VfR Laboe', '2018-09-15 14:00:00', 1, 'Sportzentrum Platz4', '4:1', '2:0', NULL),
(59, 'Verbandsliga Ost', 8, 'TSV Stein', 'SVE Comet Kiel', '2018-09-15 14:00:00', 1, 'Sportplatz Stein', '2:2', '0:2', NULL),
(60, 'Verbandsliga Ost', 8, '1. FC Schinkel', 'Dobersdorfer SV', '2018-09-15 15:00:00', 1, 'A-Platz Schinkel', '1:3', '0:2', NULL),
(61, 'Verbandsliga Ost', 8, 'MTV Dänischenhagen', 'ASV Dersau', '2018-09-15 15:30:00', 1, 'Sportanlage Dänischenhagen	', '4:2', '1:0', NULL),
(62, 'Verbandsliga Ost', 8, 'SSG Rot-Schwarz Kiel', 'Preetzer TSV', '2018-09-15 16:00:00', 1, 'Sportplatz Meimersdorf', '7:2', '2:0', NULL),
(63, 'Verbandsliga Ost', 8, 'SG Insel Fehmarn I', 'FC Kilia Kiel', '2018-09-16 15:00:00', 1, 'RSV1', '4:1', '0:1', NULL),
(64, 'Verbandsliga Ost', 8, 'TSV Malente', 'Eutin 08 II', '2018-09-16 15:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '1:1', '0:1', NULL),
(65, 'Verbandsliga Ost', 9, 'SVE Comet Kiel', 'MTV Dänischenhagen', '2018-09-22 14:00:00', 1, 'Ellerbek Platz A', '1:0', '1:0', NULL),
(66, 'Verbandsliga Ost', 9, 'FC Kilia Kiel', 'Preetzer TSV', '2018-09-22 15:00:00', 1, 'Hauptplatz', '0:2', '0:2', NULL),
(67, 'Verbandsliga Ost', 9, 'VfR Laboe', '1. FC Schinkel', '2018-09-22 15:00:00', 1, 'Stoschplatz Hauptplatz', '2:2', '2:2', NULL),
(68, 'Verbandsliga Ost', 9, 'SG Insel Fehmarn I', 'TSV Stein', '2018-09-22 15:00:00', 1, 'RSV1', '0:2', '0:0', NULL),
(69, 'Verbandsliga Ost', 9, 'ASV Dersau', 'TSV Plön', '2018-09-22 15:00:00', 1, 'A-Platz Dersau', '1:1', '1:0', NULL),
(70, 'Verbandsliga Ost', 9, 'Dobersdorfer SV', 'TSV Malente', '2018-09-23 15:00:00', 1, 'Sportplatz Dieter von Bortstel	', '2:1', '0:1', NULL),
(71, 'Verbandsliga Ost', 9, 'Eutin 08 II', 'SSG Rot-Schwarz Kiel', '2018-09-23 15:00:00', 1, 'Fritz-Latendorf-Stadion ', '4:2', '2:0', NULL),
(72, 'Verbandsliga Ost', 9, 'SG Bornhöved/Schmalensee', 'TSV Kronshagen', '2018-09-23 15:00:00', 1, 'Seeweg Bornhoeved A-Platz', '2:7', '0:3', NULL),
(73, 'Verbandsliga Ost', 10, 'TSV Kronshagen', 'ASV Dersau', '2018-09-29 14:00:00', 1, 'Sportzentrum Platz4', '4:0', '3:0', NULL),
(74, 'Verbandsliga Ost', 10, 'TSV Plön', 'SVE Comet Kiel', '2018-09-29 14:00:00', 1, 'Schiffsthal Stadion', '1:2', '1:1', NULL),
(75, 'Verbandsliga Ost', 10, 'TSV Stein', 'FC Kilia Kiel', '2018-09-29 14:00:00', 1, 'Sportplatz Stein', '6:0', '0:0', NULL),
(76, 'Verbandsliga Ost', 10, '1. FC Schinkel', 'SG Bornhöved/Schmalensee', '2018-09-29 15:00:00', 1, 'A-Platz Schinkel', '1:6', '1:2', NULL),
(77, 'Verbandsliga Ost', 10, 'MTV Dänischenhagen', 'SG Insel Fehmarn I', '2018-09-29 15:30:00', 1, 'Sportanlage Dänischenhagen	', '1:0', '1:0', NULL),
(78, 'Verbandsliga Ost', 10, 'Preetzer TSV', 'Eutin 08 II', '2018-09-29 15:30:00', 1, 'Stadion Preetz', '3:2', '2:2', NULL),
(79, 'Verbandsliga Ost', 10, 'SSG Rot-Schwarz Kiel', 'Dobersdorfer SV', '2018-09-29 16:00:00', 1, 'Sportplatz Meimersdorf', '1:1', '0:0', NULL),
(80, 'Verbandsliga Ost', 10, 'TSV Malente', 'VfR Laboe', '2018-09-30 15:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '0:6', '0:3', NULL),
(81, 'Verbandsliga Ost', 11, 'TSV Stein', 'MTV Dänischenhagen', '2018-10-06 14:00:00', 1, 'Sportplatz Stein', '0:2', '0:1', NULL),
(82, 'Verbandsliga Ost', 11, 'SVE Comet Kiel', 'TSV Kronshagen', '2018-10-06 14:00:00', 1, 'Ellerbek Platz A', '3:2', '2:1', NULL),
(83, 'Verbandsliga Ost', 11, 'FC Kilia Kiel', 'Eutin 08 II', '2018-10-06 15:00:00', 1, 'Hauptplatz', '2:3', '0:1', NULL),
(84, 'Verbandsliga Ost', 11, 'VfR Laboe', 'SSG Rot-Schwarz Kiel', '2018-10-06 15:00:00', 1, 'Stoschplatz Hauptplatz', '2:0', '0:0', NULL),
(85, 'Verbandsliga Ost', 11, 'ASV Dersau', '1. FC Schinkel', '2018-10-06 15:00:00', 1, 'A-Platz Dersau', '1:1', '1:0', NULL),
(86, 'Verbandsliga Ost', 11, 'SG Bornhöved/Schmalensee', 'TSV Malente', '2018-10-06 15:00:00', 1, 'Seeweg Bornhoeved A-Platz', '7:1', '2:0', NULL),
(87, 'Verbandsliga Ost', 11, 'Dobersdorfer SV', 'Preetzer TSV', '2018-10-07 15:00:00', 1, 'Sportplatz Dieter von Bortstel	', '1:1', '1:0', NULL),
(88, 'Verbandsliga Ost', 11, 'SG Insel Fehmarn I', 'TSV Plön', '2018-10-07 15:00:00', 1, 'RSV1', '1:0', '0:0', NULL),
(89, 'Verbandsliga Ost', 12, 'TSV Plön', 'TSV Stein', '2018-10-12 19:00:00', 1, 'Schiffsthal Stadion', '0:2', '0:1', NULL),
(90, 'Verbandsliga Ost', 12, 'TSV Kronshagen', 'SG Insel Fehmarn I', '2018-10-13 14:00:00', 1, 'Sportzentrum Platz4', '4:1', '3:0', NULL),
(91, 'Verbandsliga Ost', 12, '1. FC Schinkel', 'SVE Comet Kiel', '2018-10-13 15:00:00', 1, 'A-Platz Schinkel', '2:5', '1:2', NULL),
(92, 'Verbandsliga Ost', 12, 'MTV Dänischenhagen', 'FC Kilia Kiel', '2018-10-13 15:30:00', 1, 'Sportanlage Dänischenhagen	', '4:0', '1:0', NULL),
(93, 'Verbandsliga Ost', 12, 'Preetzer TSV', 'VfR Laboe', '2018-10-13 15:30:00', 1, 'Stadion Preetz', '0:3', '0:2', NULL),
(94, 'Verbandsliga Ost', 12, 'SSG Rot-Schwarz Kiel', 'SG Bornhöved/Schmalensee', '2018-10-13 15:30:00', 1, 'Sportplatz Meimersdorf', '6:2', '3:1', NULL),
(95, 'Verbandsliga Ost', 12, 'Eutin 08 II', 'Dobersdorfer SV', '2018-10-14 15:00:00', 1, 'Fritz-Latendorf-Stadion ', '1:3', '1:1', NULL),
(96, 'Verbandsliga Ost', 12, 'TSV Malente', 'ASV Dersau', '2018-10-14 15:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '0:1', '0:1', NULL),
(97, 'Verbandsliga Ost', 13, 'TSV Stein', 'TSV Kronshagen', '2018-10-20 14:00:00', 1, 'Sportplatz Stein', '1:3', '1:1', NULL),
(98, 'Verbandsliga Ost', 13, 'SVE Comet Kiel', 'TSV Malente', '2018-10-20 14:00:00', 1, 'Ellerbek Platz A', '4:0', '0:0', NULL),
(99, 'Verbandsliga Ost', 13, 'FC Kilia Kiel', 'Dobersdorfer SV', '2018-10-20 15:00:00', 1, 'Hauptplatz', '0:4', '0:1', NULL),
(100, 'Verbandsliga Ost', 13, 'VfR Laboe', 'Eutin 08 II', '2018-10-20 15:00:00', 1, 'Stoschplatz Hauptplatz', '3:3', '1:3', NULL),
(101, 'Verbandsliga Ost', 13, 'ASV Dersau', 'SSG Rot-Schwarz Kiel', '2018-10-20 15:00:00', 1, 'A-Platz Dersau', '0:6', '0:3', NULL),
(102, 'Verbandsliga Ost', 13, 'MTV Dänischenhagen', 'TSV Plön', '2018-10-20 15:30:00', 1, 'Sportanlage Dänischenhagen	', '1:2', '1:0', NULL),
(103, 'Verbandsliga Ost', 13, 'SG Insel Fehmarn I', '1. FC Schinkel', '2018-10-21 14:00:00', 1, 'RSV1', '0:1', '0:1', NULL),
(104, 'Verbandsliga Ost', 13, 'SG Bornhöved/Schmalensee', 'Preetzer TSV', '2018-10-21 15:00:00', 1, 'Seeweg Bornhoeved A-Platz', '3:0', '0:0', NULL),
(105, 'Verbandsliga Ost', 14, 'TSV Kronshagen', 'MTV Dänischenhagen', '2018-10-27 14:00:00', 1, 'Sportzentrum Platz4', '1:1', '1:1', NULL),
(106, 'Verbandsliga Ost', 14, 'TSV Plön', 'FC Kilia Kiel', '2018-10-27 14:00:00', 1, 'Schiffsthal Stadion', '2:0', '0:0', NULL),
(107, 'Verbandsliga Ost', 14, 'Preetzer TSV', 'ASV Dersau', '2018-10-27 14:00:00', 1, 'Stadion Preetz', '1:4', '0:1', NULL),
(108, 'Verbandsliga Ost', 14, 'SSG Rot-Schwarz Kiel', 'SVE Comet Kiel', '2018-10-27 14:00:00', 1, 'Sportplatz Meimersdorf', '3:3', '2:1', NULL),
(109, 'Verbandsliga Ost', 14, '1. FC Schinkel', 'TSV Stein', '2018-10-27 14:00:00', 1, 'A-Platz Schinkel', '3:4', '1:2', NULL),
(110, 'Verbandsliga Ost', 14, 'Dobersdorfer SV', 'SG Bornhöved/Schmalensee', '2018-10-28 14:00:00', 1, 'Sportplatz Dieter von Bortstel	', '2:0', '0:0', NULL),
(111, 'Verbandsliga Ost', 14, 'TSV Malente', 'SG Insel Fehmarn I', '2018-10-28 14:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '1:2', '1:1', NULL),
(112, 'Verbandsliga Ost', 14, 'Eutin 08 II', 'SG Bornhöved/Schmalensee', '2018-10-28 15:00:00', 1, 'Fritz-Latendorf-Stadion ', '1:1', '0:0', NULL),
(113, 'Verbandsliga Ost', 15, 'FC Kilia Kiel', 'VfR Laboe', '2018-11-03 14:00:00', 1, 'Hauptplatz', '1:3', '1:2', NULL),
(114, 'Verbandsliga Ost', 15, 'TSV Plön', 'TSV Kronshagen', '2018-11-03 14:00:00', 1, 'Schiffsthal Stadion', '1:4', '0:2', NULL),
(115, 'Verbandsliga Ost', 15, 'MTV Dänischenhagen', '1. FC Schinkel', '2018-11-03 14:00:00', 1, 'Sportanlage Dänischenhagen	', '5:1', '2:1', NULL),
(116, 'Verbandsliga Ost', 15, 'TSV Stein', 'TSV Malente', '2018-11-03 14:00:00', 1, 'Sportplatz Stein', '1:3', '0:1', NULL),
(117, 'Verbandsliga Ost', 15, 'SVE Comet Kiel', 'Preetzer TSV', '2018-11-03 14:00:00', 1, 'Ellerbek Platz A', '2:0', '1:0', NULL),
(118, 'Verbandsliga Ost', 15, 'ASV Dersau', 'Eutin 08 II', '2018-11-03 14:00:00', 1, 'A-Platz Dersau', '5:0', '5:0', NULL),
(119, 'Verbandsliga Ost', 15, 'SG Insel Fehmarn I', 'SSG Rot-Schwarz Kiel', '2018-11-04 14:00:00', 1, 'RSV1', '2:3', '0:3', NULL),
(120, 'Verbandsliga Ost', 15, 'SG Bornhöved/Schmalensee', 'Dobersdorfer SV', '2018-11-04 14:00:00', 1, 'Seeweg Bornhoeved A-Platz', '2:1', '2:0', NULL),
(121, 'Verbandsliga Ost', 16, 'SVE Comet Kiel', 'VfR Laboe', '2018-11-10 14:00:00', 1, 'Ellerbek Platz A', '2:0', '1:0', NULL),
(122, 'Verbandsliga Ost', 16, 'TSV Stein', 'Eutin 08 II', '2018-11-10 14:00:00', 1, 'Sportplatz Stein', '1:1', '0:1', NULL),
(123, 'Verbandsliga Ost', 16, 'MTV Dänischenhagen', 'Preetzer TSV', '2018-11-10 14:00:00', 1, 'Sportanlage Dänischenhagen	', '3:1', '3:0', NULL),
(124, 'Verbandsliga Ost', 16, 'SSG Rot-Schwarz Kiel', 'TSV Plön', '2018-11-10 14:00:00', 1, 'Sportplatz Meimersdorf', '2:5', '2:1', NULL),
(125, 'Verbandsliga Ost', 16, 'TSV Kronshagen', 'TSV Malente', '2018-11-10 14:00:00', 1, 'Sportzentrum Platz4', '4:0', '1:0', NULL),
(126, 'Verbandsliga Ost', 16, '1. FC Schinkel', 'FC Kilia Kiel', '2018-11-11 14:00:00', 1, 'A-Platz Schinkel', '2:1', '1:0', NULL),
(127, 'Verbandsliga Ost', 16, 'SG Insel Fehmarn I', 'Dobersdorfer SV', '2018-11-11 14:00:00', 1, 'RSV1', '1:4', '0:2', NULL),
(128, 'Verbandsliga Ost', 16, 'SG Bornhöved/Schmalensee', 'ASV Dersau', '2018-11-11 14:00:00', 1, 'Seeweg Bornhoeved A-Platz', '2:0', '1:0', NULL),
(129, 'Verbandsliga Ost', 17, '1. FC Schinkel', 'TSV Kronshagen', '2018-11-17 14:00:00', 1, 'A-Platz Schinkel', '0:5', '0:0', NULL),
(130, 'Verbandsliga Ost', 17, 'SSG Rot-Schwarz Kiel', 'MTV Dänischenhagen', '2018-11-17 14:00:00', 1, 'Sportplatz Meimersdorf', '4:1', '1:0', NULL),
(131, 'Verbandsliga Ost', 17, 'Preetzer TSV', 'TSV Stein', '2019-03-03 14:00:00', 0, 'Stadion Preetz', NULL, NULL, NULL),
(132, 'Verbandsliga Ost', 17, 'VfR Laboe', 'ASV Dersau', '2018-11-17 14:00:00', 1, 'Stoschplatz Hauptplatz', '5:0', '2:0', NULL),
(133, 'Verbandsliga Ost', 17, 'FC Kilia Kiel', 'SG Bornhöved/Schmalensee', '2018-11-17 14:00:00', 1, 'Hauptplatz', '3:0', '0:0', NULL),
(134, 'Verbandsliga Ost', 17, 'TSV Malente', 'TSV Plön', '2018-11-18 14:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '1:5', '1:2', NULL),
(135, 'Verbandsliga Ost', 17, 'Dobersdorfer SV', 'SVE Comet Kiel', '2018-11-18 14:00:00', 1, 'Sportplatz Dieter von Bortstel	', '1:3', '0:1', NULL),
(136, 'Verbandsliga Ost', 17, 'Eutin 08 II', 'SG Insel Fehmarn I', '2018-11-18 15:00:00', 1, 'Eutin Kunstrasen', '2:0', '0:0', NULL),
(137, 'Verbandsliga Ost', 18, 'FC Kilia Kiel', 'TSV Malente', '2018-11-24 14:00:00', 1, 'Hauptplatz', '3:1', '0:1', NULL),
(138, 'Verbandsliga Ost', 18, 'TSV Stein', 'VfR Laboe', '2018-11-24 14:00:00', 1, 'Sportplatz Stein', '3:0', '1:0', NULL),
(139, 'Verbandsliga Ost', 18, 'MTV Dänischenhagen', 'Dobersdorfer SV', '2018-11-24 14:00:00', 1, 'Sportanlage Dänischenhagen	', '1:1', '0:0', NULL),
(140, 'Verbandsliga Ost', 18, 'TSV Plön', 'Eutin 08 II', '2018-11-24 14:00:00', 1, 'Schiffsthal Stadion', '5:1', '3:1', NULL),
(141, 'Verbandsliga Ost', 18, 'TSV Kronshagen', 'Preetzer TSV', '2018-11-24 14:00:00', 1, 'Sportzentrum Platz4', '6:0', '0:0', NULL),
(142, 'Verbandsliga Ost', 18, 'SSG Rot-Schwarz Kiel', '1. FC Schinkel', '2018-11-24 14:00:00', 1, 'Sportplatz Meimersdorf', '4:2', '2:0', NULL),
(143, 'Verbandsliga Ost', 18, 'SVE Comet Kiel', 'ASV Dersau', '2018-11-24 14:00:00', 1, 'Ellerbek Platz A', '2:1', '0:1', NULL),
(144, 'Verbandsliga Ost', 18, 'SG Insel Fehmarn I', 'SG Bornhöved/Schmalensee', '2018-11-25 14:00:00', 1, 'RSV1', '2:1', '2:0', NULL),
(145, 'Verbandsliga Ost', 19, 'SSG Rot-Schwarz Kiel', 'TSV Kronshagen', '2019-03-02 14:00:00', 0, 'Sportplatz Meimersdorf', NULL, NULL, NULL),
(146, 'Verbandsliga Ost', 19, 'Preetzer TSV', 'TSV Plön', '2019-04-03 19:00:00', 0, 'Stadion Preetz', NULL, NULL, NULL),
(147, 'Verbandsliga Ost', 19, 'TSV Stein', 'Dobersdorfer SV', '2018-12-01 14:00:00', 1, 'Sportplatz Stein', '2:1', '1:1', NULL),
(148, 'Verbandsliga Ost', 19, 'VfR Laboe', 'SG Insel Fehmarn I', '2018-12-01 14:00:00', 1, 'Stoschplatz Hauptplatz', '1:4', '1:4', NULL),
(149, 'Verbandsliga Ost', 19, 'FC Kilia Kiel', 'ASV Dersau', '2019-03-02 14:00:00', 0, 'Hauptplatz', NULL, NULL, NULL),
(150, 'Verbandsliga Ost', 19, 'SG Bornhöved/Schmalensee', 'SVE Comet Kiel', '2019-03-03 14:00:00', 0, 'Seeweg Bornhoeved A-Platz', NULL, NULL, NULL),
(151, 'Verbandsliga Ost', 19, 'TSV Malente', '1. FC Schinkel', '2019-03-03 14:00:00', 0, 'Stadion Ernst-Ruediger-Sportzentrum', NULL, NULL, NULL),
(152, 'Verbandsliga Ost', 19, 'Eutin 08 II', 'MTV Dänischenhagen', '2019-03-02 14:00:00', 0, 'Eutin Kunstrasen', NULL, NULL, NULL),
(153, 'Verbandsliga Ost', 20, 'SVE Comet Kiel', 'TSV Stein', '2019-03-09 14:00:00', 0, 'Ellerbek Platz A', NULL, NULL, NULL),
(154, 'Verbandsliga Ost', 20, 'VfR Laboe', 'TSV Kronshagen', '2019-03-09 15:00:00', 0, 'Stoschplatz Hauptplatz', NULL, NULL, NULL),
(155, 'Verbandsliga Ost', 20, 'ASV Dersau', 'MTV Dänischenhagen', '2019-03-09 15:00:00', 0, 'A-Platz Dersau', NULL, NULL, NULL),
(156, 'Verbandsliga Ost', 20, 'FC Kilia Kiel', 'SG Insel Fehmarn I', '2019-03-09 15:00:00', 0, 'Hauptplatz', NULL, NULL, NULL),
(157, 'Verbandsliga Ost', 20, 'Preetzer TSV', 'SSG Rot-Schwarz Kiel', '2019-03-09 15:30:00', 0, 'Stadion Preetz', NULL, NULL, NULL),
(158, 'Verbandsliga Ost', 20, 'SG Bornhöved/Schmalensee', 'TSV Plön', '2019-03-10 15:00:00', 0, 'Seeweg Bornhoeved A-Platz', NULL, NULL, NULL),
(159, 'Verbandsliga Ost', 20, 'Eutin 08 II', 'TSV Malente', '2019-03-10 15:00:00', 0, 'Eutin Kunstrasen', NULL, NULL, NULL),
(160, 'Verbandsliga Ost', 20, 'Dobersdorfer SV', '1. FC Schinkel', '2019-03-10 15:00:00', 0, 'Sportplatz Dieter von Bortstel	', NULL, NULL, NULL),
(161, 'Verbandsliga Ost', 21, 'TSV Stein', 'SG Insel Fehmarn I', '2019-03-16 14:00:00', 0, 'Sportplatz Stein', NULL, NULL, NULL),
(162, 'Verbandsliga Ost', 21, 'TSV Kronshagen', 'SG Bornhöved/Schmalensee', '2019-03-16 14:00:00', 0, 'Sportzentrum Platz4', NULL, NULL, NULL),
(163, 'Verbandsliga Ost', 21, '1. FC Schinkel', 'VfR Laboe', '2019-03-16 15:00:00', 0, 'A-Platz Schinkel', NULL, NULL, NULL),
(164, 'Verbandsliga Ost', 21, 'TSV Plön', 'ASV Dersau', '2019-03-16 15:00:00', 0, 'Schiffsthal Stadion', NULL, NULL, NULL),
(165, 'Verbandsliga Ost', 21, 'Preetzer TSV', 'FC Kilia Kiel', '2019-03-16 15:30:00', 0, 'Stadion Preetz', NULL, NULL, NULL),
(166, 'Verbandsliga Ost', 21, 'MTV Dänischenhagen', 'SVE Comet Kiel', '2019-03-16 15:30:00', 0, 'Sportanlage Dänischenhagen	', NULL, NULL, NULL),
(167, 'Verbandsliga Ost', 21, 'SSG Rot-Schwarz Kiel', 'Eutin 08 II', '2019-03-16 16:00:00', 0, 'Sportplatz Meimersdorf', NULL, NULL, NULL),
(168, 'Verbandsliga Ost', 21, 'TSV Malente', 'Dobersdorfer SV', '2019-03-17 15:00:00', 0, 'Stadion Ernst-Ruediger-Sportzentrum', NULL, NULL, NULL),
(169, 'Verbandsliga Ost', 22, 'SVE Comet Kiel', 'TSV Plön', '2019-03-23 14:00:00', 0, 'Ellerbek Platz A', NULL, NULL, NULL),
(170, 'Verbandsliga Ost', 22, 'ASV Dersau', 'TSV Kronshagen', '2019-03-23 15:00:00', 0, 'A-Platz Dersau', NULL, NULL, NULL),
(171, 'Verbandsliga Ost', 22, 'FC Kilia Kiel', 'TSV Stein', '2019-03-23 15:00:00', 0, 'Hauptplatz', NULL, NULL, NULL),
(172, 'Verbandsliga Ost', 22, 'VfR Laboe', 'TSV Malente', '2019-03-23 15:00:00', 0, 'Stoschplatz Hauptplatz', NULL, NULL, NULL),
(173, 'Verbandsliga Ost', 22, 'SG Insel Fehmarn I', 'MTV Dänischenhagen', '2019-03-24 15:00:00', 0, 'RSV1', NULL, NULL, NULL),
(174, 'Verbandsliga Ost', 22, 'Eutin 08 II', 'Preetzer TSV', '2019-03-24 15:00:00', 0, 'Fritz-Latendorf-Stadion ', NULL, NULL, NULL),
(175, 'Verbandsliga Ost', 22, 'Dobersdorfer SV', 'SSG Rot-Schwarz Kiel', '2019-03-24 15:00:00', 0, 'Sportplatz Dieter von Bortstel	', NULL, NULL, NULL),
(176, 'Verbandsliga Ost', 22, 'SG Bornhöved/Schmalensee', '1. FC Schinkel', '2019-03-24 15:00:00', 0, 'Seeweg Bornhoeved A-Platz', NULL, NULL, NULL),
(177, 'Verbandsliga Ost', 23, 'TSV Kronshagen', 'SVE Comet Kiel', '2019-03-30 14:00:00', 0, 'Sportzentrum Platz4', NULL, NULL, NULL),
(178, 'Verbandsliga Ost', 23, 'TSV Plön', 'SG Insel Fehmarn I', '2019-03-30 15:00:00', 0, 'Schiffsthal Stadion', NULL, NULL, NULL),
(179, 'Verbandsliga Ost', 23, '1. FC Schinkel', 'ASV Dersau', '2019-03-30 15:00:00', 0, 'A-Platz Schinkel', NULL, NULL, NULL),
(180, 'Verbandsliga Ost', 23, 'Preetzer TSV', 'Dobersdorfer SV', '2019-03-30 15:30:00', 0, 'Stadion Preetz', NULL, NULL, NULL),
(181, 'Verbandsliga Ost', 23, 'MTV Dänischenhagen', 'TSV Stein', '2019-03-30 15:30:00', 0, 'Sportanlage Dänischenhagen	', NULL, NULL, NULL),
(182, 'Verbandsliga Ost', 23, 'SSG Rot-Schwarz Kiel', 'VfR Laboe', '2019-03-30 16:00:00', 0, 'Sportplatz Meimersdorf', NULL, NULL, NULL),
(183, 'Verbandsliga Ost', 23, 'Eutin 08 II', 'FC Kilia Kiel', '2019-03-31 15:00:00', 0, 'Fritz-Latendorf-Stadion ', NULL, NULL, NULL),
(184, 'Verbandsliga Ost', 23, 'TSV Malente', 'SG Bornhöved/Schmalensee', '2019-03-31 15:00:00', 0, 'Stadion Ernst-Ruediger-Sportzentrum', NULL, NULL, NULL),
(185, 'Verbandsliga Ost', 24, 'TSV Stein', 'TSV Plön', '2019-04-06 14:00:00', 0, 'Sportplatz Stein', NULL, NULL, NULL),
(186, 'Verbandsliga Ost', 24, 'SVE Comet Kiel', '1. FC Schinkel', '2019-04-06 14:00:00', 0, 'A-Platz SVE Comet Kiel', NULL, NULL, NULL),
(187, 'Verbandsliga Ost', 24, 'SG Insel Fehmarn I', 'TSV Kronshagen', '2019-04-06 15:00:00', 0, 'RSV1', NULL, NULL, NULL),
(188, 'Verbandsliga Ost', 24, 'FC Kilia Kiel', 'MTV Dänischenhagen', '2019-04-06 15:00:00', 0, 'Hauptplatz', NULL, NULL, NULL),
(189, 'Verbandsliga Ost', 24, 'VfR Laboe', 'Preetzer TSV', '2019-04-06 15:00:00', 0, 'Stoschplatz Hauptplatz', NULL, NULL, NULL),
(190, 'Verbandsliga Ost', 24, 'ASV Dersau', 'TSV Malente', '2019-04-06 15:00:00', 0, 'A-Platz Dersau', NULL, NULL, NULL),
(191, 'Verbandsliga Ost', 24, 'Dobersdorfer SV', 'Eutin 08 II', '2019-04-07 15:00:00', 0, 'Sportplatz Dieter von Bortstel	', NULL, NULL, NULL),
(192, 'Verbandsliga Ost', 24, 'SG Bornhöved/Schmalensee', 'SSG Rot-Schwarz Kiel', '2019-04-07 15:00:00', 0, 'Seeweg Bornhoeved A-Platz', NULL, NULL, NULL),
(193, 'Verbandsliga Ost', 25, 'TSV Kronshagen', 'TSV Stein', '2019-04-13 14:00:00', 0, 'Sportzentrum Platz4', NULL, NULL, NULL),
(194, 'Verbandsliga Ost', 25, 'TSV Plön', 'MTV Dänischenhagen', '2019-04-13 15:00:00', 0, 'Schiffsthal Stadion', NULL, NULL, NULL),
(195, 'Verbandsliga Ost', 25, '1. FC Schinkel', 'SG Insel Fehmarn I', '2019-04-13 15:00:00', 0, 'A-Platz Schinkel', NULL, NULL, NULL),
(196, 'Verbandsliga Ost', 25, 'Preetzer TSV', 'SG Bornhöved/Schmalensee', '2019-04-13 15:30:00', 0, 'Stadion Preetz', NULL, NULL, NULL),
(197, 'Verbandsliga Ost', 25, 'SSG Rot-Schwarz Kiel', 'ASV Dersau', '2019-04-13 16:00:00', 0, 'Sportplatz Meimersdorf', NULL, NULL, NULL),
(198, 'Verbandsliga Ost', 25, 'Dobersdorfer SV', 'FC Kilia Kiel', '2019-04-14 15:00:00', 0, 'Sportplatz Dieter von Bortstel	', NULL, NULL, NULL),
(199, 'Verbandsliga Ost', 25, 'Eutin 08 II', 'VfR Laboe', '2019-04-14 15:00:00', 0, 'Fritz-Latendorf-Stadion ', NULL, NULL, NULL),
(200, 'Verbandsliga Ost', 25, 'TSV Malente', 'SVE Comet Kiel', '2019-04-14 15:00:00', 0, 'Stadion Ernst-Ruediger-Sportzentrum', NULL, NULL, NULL),
(201, 'Verbandsliga Ost', 26, 'SVE Comet Kiel', 'SSG Rot-Schwarz Kiel', '2019-04-27 14:00:00', 0, 'A-Platz SVE Comet Kiel', NULL, NULL, NULL),
(202, 'Verbandsliga Ost', 26, 'TSV Stein', '1. FC Schinkel', '2019-04-27 14:00:00', 0, 'Sportplatz Stein', NULL, NULL, NULL),
(203, 'Verbandsliga Ost', 26, 'FC Kilia Kiel', 'TSV Plön', '2019-04-27 15:00:00', 0, 'Hauptplatz', NULL, NULL, NULL),
(204, 'Verbandsliga Ost', 26, 'VfR Laboe', 'Dobersdorfer SV', '2019-04-27 15:00:00', 0, 'Stoschplatz Hauptplatz', NULL, NULL, NULL),
(205, 'Verbandsliga Ost', 26, 'ASV Dersau', 'Preetzer TSV', '2019-04-27 15:00:00', 0, 'A-Platz Dersau', NULL, NULL, NULL),
(206, 'Verbandsliga Ost', 26, 'MTV Dänischenhagen', 'TSV Kronshagen', '2019-04-27 15:30:00', 0, 'Sportanlage Dänischenhagen	', NULL, NULL, NULL),
(207, 'Verbandsliga Ost', 26, 'SG Bornhöved/Schmalensee', 'Eutin 08 II', '2019-04-28 15:00:00', 0, 'Seeweg Bornhoeved A-Platz', NULL, NULL, NULL),
(208, 'Verbandsliga Ost', 26, 'SG Insel Fehmarn I', 'TSV Malente', '2019-04-28 15:00:00', 0, 'RSV1', NULL, NULL, NULL),
(209, 'Verbandsliga Ost', 27, 'TSV Kronshagen', 'TSV Plön', '2019-05-04 14:00:00', 0, 'Sportzentrum Platz4', NULL, NULL, NULL),
(210, 'Verbandsliga Ost', 27, 'VfR Laboe', 'FC Kilia Kiel', '2019-05-04 15:00:00', 0, 'Stoschplatz Hauptplatz', NULL, NULL, NULL),
(211, 'Verbandsliga Ost', 27, '1. FC Schinkel', 'MTV Dänischenhagen', '2019-05-04 15:00:00', 0, 'A-Platz Schinkel', NULL, NULL, NULL),
(212, 'Verbandsliga Ost', 27, 'Preetzer TSV', 'SVE Comet Kiel', '2019-05-04 15:30:00', 0, 'Stadion Preetz', NULL, NULL, NULL),
(213, 'Verbandsliga Ost', 27, 'SSG Rot-Schwarz Kiel', 'SG Insel Fehmarn I', '2019-05-04 16:00:00', 0, 'Sportplatz Meimersdorf', NULL, NULL, NULL),
(214, 'Verbandsliga Ost', 27, 'TSV Malente', 'TSV Stein', '2019-05-05 15:00:00', 0, 'Stadion Ernst-Ruediger-Sportzentrum', NULL, NULL, NULL),
(215, 'Verbandsliga Ost', 27, 'Eutin 08 II', 'ASV Dersau', '2019-05-05 15:00:00', 0, 'Fritz-Latendorf-Stadion ', NULL, NULL, NULL),
(216, 'Verbandsliga Ost', 27, 'Dobersdorfer SV', 'SG Bornhöved/Schmalensee', '2019-05-05 15:00:00', 0, 'Sportplatz Dieter von Bortstel	', NULL, NULL, NULL),
(217, 'Verbandsliga Ost', 28, 'TSV Kronshagen', 'Dobersdorfer SV', '2019-05-11 14:00:00', 0, 'Sportzentrum Platz4', NULL, NULL, NULL),
(218, 'Verbandsliga Ost', 28, 'TSV Stein', 'ASV Dersau', '2019-05-11 14:00:00', 0, 'Sportplatz Stein', NULL, NULL, NULL),
(219, 'Verbandsliga Ost', 28, 'TSV Plön', 'VfR Laboe', '2019-05-11 15:00:00', 0, 'Schiffsthal Stadion', NULL, NULL, NULL),
(220, 'Verbandsliga Ost', 28, '1. FC Schinkel', 'Eutin 08 II', '2019-05-11 15:00:00', 0, 'A-Platz Schinkel', NULL, NULL, NULL),
(221, 'Verbandsliga Ost', 28, 'MTV Dänischenhagen', 'SG Bornhöved/Schmalensee', '2019-05-11 15:30:00', 0, 'Sportanlage Dänischenhagen	', NULL, NULL, NULL),
(222, 'Verbandsliga Ost', 28, 'SSG Rot-Schwarz Kiel', 'FC Kilia Kiel', '2019-05-11 16:00:00', 0, 'Sportplatz Meimersdorf', NULL, NULL, NULL),
(223, 'Verbandsliga Ost', 28, 'TSV Malente', 'Preetzer TSV', '2019-05-12 15:00:00', 0, 'Stadion Ernst-Ruediger-Sportzentrum', NULL, NULL, NULL),
(224, 'Verbandsliga Ost', 28, 'SG Insel Fehmarn I', 'SVE Comet Kiel', '2019-05-12 15:00:00', 0, 'RSV1', NULL, NULL, NULL),
(225, 'Verbandsliga Ost', 29, 'VfR Laboe', '1. FC Schinkel', '2019-05-18 15:00:00', 0, 'Stoschplatz Hauptplatz', NULL, NULL, NULL),
(226, 'Verbandsliga Ost', 29, 'ASV Dersau', 'SG Insel Fehmarn I', '2019-05-18 15:00:00', 0, 'A-Platz Dersau', NULL, NULL, NULL),
(227, 'Verbandsliga Ost', 29, 'FC Kilia Kiel', 'SVE Comet Kiel', '2019-05-18 15:00:00', 0, 'Hauptplatz', NULL, NULL, NULL),
(228, 'Verbandsliga Ost', 29, 'Preetzer TSV', '1. FC Schinkel', '2019-05-18 15:30:00', 0, 'Stadion Preetz', NULL, NULL, NULL),
(229, 'Verbandsliga Ost', 29, 'SSG Rot-Schwarz Kiel', 'TSV Malente', '2019-05-18 16:00:00', 0, 'Sportplatz Meimersdorf', NULL, NULL, NULL),
(230, 'Verbandsliga Ost', 29, 'Eutin 08 II', 'TSV Kronshagen', '2019-05-19 15:00:00', 0, 'Fritz-Latendorf-Stadion ', NULL, NULL, NULL),
(231, 'Verbandsliga Ost', 29, 'Dobersdorfer SV', 'TSV Plön', '2019-05-19 15:00:00', 0, 'Sportplatz Dieter von Bortstel	', NULL, NULL, NULL),
(232, 'Verbandsliga Ost', 29, 'SG Bornhöved/Schmalensee', 'TSV Stein', '2019-05-19 15:00:00', 0, 'Seeweg Bornhoeved A-Platz', NULL, NULL, NULL),
(233, 'Verbandsliga Ost', 30, 'TSV Kronshagen', 'FC Kilia Kiel', '2019-05-24 19:00:00', 0, 'Sportzentrum Platz4', NULL, NULL, NULL),
(234, 'Verbandsliga Ost', 30, 'SG Bornhöved/Schmalensee', 'VfR Laboe', '2019-05-24 19:00:00', 0, 'Seeweg Bornhoeved A-Platz', NULL, NULL, NULL),
(235, 'Verbandsliga Ost', 30, 'ASV Dersau', 'Dobersdorfer SV', '2019-05-24 19:00:00', 0, 'A-Platz Dersau', NULL, NULL, NULL),
(236, 'Verbandsliga Ost', 30, 'SVE Comet Kiel', 'Eutin 08 II', '2019-05-24 19:00:00', 0, 'A-Platz SVE Comet Kiel', NULL, NULL, NULL),
(237, 'Verbandsliga Ost', 30, 'SG Insel Fehmarn I', 'Preetzer TSV', '2019-05-24 19:00:00', 0, 'RSV1', NULL, NULL, NULL),
(238, 'Verbandsliga Ost', 30, 'TSV Stein', 'SSG Rot-Schwarz Kiel', '2019-05-24 19:00:00', 0, 'Sportplatz Stein', NULL, NULL, NULL),
(239, 'Verbandsliga Ost', 30, 'MTV Dänischenhagen', 'TSV Malente', '2019-05-24 19:00:00', 0, 'Sportanlage Dänischenhagen	', NULL, NULL, NULL),
(240, 'Verbandsliga Ost', 30, 'TSV Plön', '1. FC Schinkel', '2019-05-24 19:00:00', 0, 'Schiffsthal Stadion', NULL, NULL, NULL),
(241, 'Kreisklasse A Ost', 1, 'TSV Malente II', 'SG Baltic 1', '2018-08-11 15:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '4:1', '1:0', 241),
(242, 'Kreisklasse A Ost', 1, 'TSV Lepahn', 'SC Cismar', '2018-08-11 17:00:00', 1, 'A-Platz Lepahn', '0:4', '0:1', NULL),
(243, 'Kreisklasse A Ost', 1, 'SG Insel Fehmarn II', 'SG Lensahn/Kabelhorst I', '2018-08-12 13:00:00', 1, 'RSV1', '2:6', '0:2', NULL),
(244, 'Kreisklasse A Ost', 1, 'Bujendorfer Spvg', 'TSV Selent', '2018-08-12 14:00:00', 1, 'Bujendorf1', '4:1', '1:1', NULL),
(245, 'Kreisklasse A Ost', 1, 'SG Scharbeutz/Süsel', 'TSV Lütjenburg II', '2018-08-12 15:00:00', 1, 'GleschendorfB-Kunstrasen', '6:1', '4:1', NULL),
(246, 'Kreisklasse A Ost', 1, 'SV Großenbrode', 'TSV Schönwalde', '2018-08-12 15:00:00', 1, 'Großenbrode2', '1:3', '1:0', NULL),
(247, 'Kreisklasse A Ost', 1, 'SV Heringsdorf', 'TSV Wentorf', '2018-08-12 15:00:00', 1, 'Hering1neu', '12:2', '5:2', NULL),
(248, 'Kreisklasse A Ost', 2, 'TSV Selent', 'TSV Lepahn', '2018-08-17 19:00:00', 1, 'OHLA-Stadion', '9:0', '5:0', NULL),
(249, 'Kreisklasse A Ost', 2, 'TSV Wentorf', 'Bujendorfer Spvg', '2018-08-18 15:30:00', 1, 'Sportplatz Wentorf', '0:3', '0:1', NULL),
(250, 'Kreisklasse A Ost', 2, 'TSV Lütjenburg II', 'TSV Malente II', '2018-08-18 16:00:00', 1, 'Rasenplatz Lütjenburg	', '3:3', '1:2', NULL),
(251, 'Kreisklasse A Ost', 2, 'TSV Schönwalde', 'SV Heringsdorf', '2018-08-19 15:00:00', 1, 'Schönwalde1', '1:2', '0:2', NULL),
(252, 'Kreisklasse A Ost', 2, 'SV Großenbrode', 'SG Lensahn/Kabelhorst I', '2018-08-19 15:00:00', 1, 'Großenbrode2', '3:3', '2:1', NULL),
(253, 'Kreisklasse A Ost', 2, 'SG Baltic 1', 'SG Insel Fehmarn II', '2018-08-19 15:00:00', 1, 'Dahme1', '5:2', '2:0', NULL),
(254, 'Kreisklasse A Ost', 2, 'SC Cismar', 'SG Scharbeutz/Süsel', '2018-08-19 15:00:00', 1, 'Gildeplatz', '6:0', '1:0', NULL),
(255, 'Kreisklasse A Ost', 3, 'TSV Wentorf', 'TSV Lepahn', '2018-08-25 15:30:00', 1, 'Sportplatz Wentorf', '3:0', '3:0', NULL),
(256, 'Kreisklasse A Ost', 3, 'TSV Lütjenburg II', 'SG Insel Fehmarn II', '2018-08-25 16:00:00', 1, 'Rasenplatz Lütjenburg	', '3:1', '1:0', NULL),
(257, 'Kreisklasse A Ost', 3, 'SG Lensahn/Kabelhorst I', 'SV Heringsdorf', '2018-08-26 12:45:00', 1, 'Lensahn Kunstrasen', '6:4', '4:0', NULL),
(258, 'Kreisklasse A Ost', 3, 'TSV Schönwalde', 'Bujendorfer Spvg', '2018-08-26 14:00:00', 1, 'Schönwalde1', '4:2', '1:2', NULL),
(259, 'Kreisklasse A Ost', 3, 'SC Cismar', 'TSV Selent', '2018-08-26 15:00:00', 1, 'Gildeplatz', '0:2', '0:1', NULL),
(260, 'Kreisklasse A Ost', 3, 'SG Baltic 1', 'SV Großenbrode', '2018-08-26 15:00:00', 1, 'Dahme1', '1:4', '1:0', NULL),
(261, 'Kreisklasse A Ost', 3, 'SG Scharbeutz/Süsel', 'TSV Malente II', '2018-08-26 15:00:00', 1, 'GleschendorfB-Kunstrasen', '3:2', '3:1', NULL),
(262, 'Kreisklasse A Ost', 4, 'TSV Selent', 'TSV Wentorf', '2018-08-31 19:00:00', 1, 'OHLA-Stadion', '1:1', '0:1', NULL),
(263, 'Kreisklasse A Ost', 4, 'TSV Malente II', 'SC Cismar', '2018-09-01 15:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '0:1', '0:0', NULL),
(264, 'Kreisklasse A Ost', 4, 'TSV Lepahn', 'TSV Schönwalde', '2018-09-01 17:00:00', 1, 'A-Platz Lepahn', '0:2', '0:1', NULL),
(265, 'Kreisklasse A Ost', 4, 'SG Insel Fehmarn II', 'SG Scharbeutz/Süsel', '2018-09-02 13:00:00', 1, 'RSV1', '4:1', '2:0', NULL),
(266, 'Kreisklasse A Ost', 4, 'Bujendorfer Spvg', 'SG Lensahn/Kabelhorst I', '2018-09-02 14:00:00', 1, 'Bujendorf1', '3:1', '1:1', NULL),
(267, 'Kreisklasse A Ost', 4, 'SV Großenbrode', 'TSV Lütjenburg II', '2018-09-02 15:00:00', 1, 'Großenbrode2', '1:0', '0:0', NULL),
(268, 'Kreisklasse A Ost', 4, 'SV Heringsdorf', 'SG Baltic 1', '2018-11-24 16:30:00', 1, 'Hering1neu', '3:1', '1:0', NULL),
(269, 'Kreisklasse A Ost', 5, 'TSV Malente II', 'SG Insel Fehmarn II', '2018-09-08 15:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '2:7', '2:1', NULL),
(270, 'Kreisklasse A Ost', 5, 'TSV Lütjenburg II', 'SV Heringsdorf', '2018-09-08 16:00:00', 1, 'Rasenplatz Lütjenburg	', '1:2', '1:1', NULL),
(271, 'Kreisklasse A Ost', 5, 'SG Lensahn/Kabelhorst I', 'TSV Lepahn', '2018-09-09 12:45:00', 1, 'Kabel1', '5:0', '5:0', NULL),
(272, 'Kreisklasse A Ost', 5, 'SC Cismar', 'TSV Wentorf', '2018-09-09 15:00:00', 1, 'Gildeplatz', '4:2', '3:1', NULL),
(273, 'Kreisklasse A Ost', 5, 'SG Baltic 1', 'Bujendorfer Spvg', '2018-09-09 15:00:00', 1, 'Dahme1', '0:7', '0:4', NULL),
(274, 'Kreisklasse A Ost', 5, 'TSV Schönwalde', 'TSV Selent', '2018-09-09 15:00:00', 1, 'Schönwalde1', '3:1', '2:1', NULL),
(275, 'Kreisklasse A Ost', 5, 'SG Scharbeutz/Süsel', 'SV Großenbrode', '2018-09-09 15:00:00', 1, 'GleschendorfB-Kunstrasen', '2:0', '1:0', NULL),
(276, 'Kreisklasse A Ost', 6, 'TSV Selent', 'SG Lensahn/Kabelhorst I', '2018-09-14 19:00:00', 1, 'OHLA-Stadion', '4:2', '2:1', NULL),
(277, 'Kreisklasse A Ost', 6, 'TSV Wentorf', 'TSV Schönwalde', '2018-09-15 15:30:00', 1, 'Sportplatz Wentorf', '3:1', '1:1', NULL),
(278, 'Kreisklasse A Ost', 6, 'TSV Lepahn', 'SG Baltic 1', '2018-09-15 17:00:00', 1, 'A-Platz Lepahn', '3:3', '1:1', NULL),
(279, 'Kreisklasse A Ost', 6, 'SG Insel Fehmarn II', 'SC Cismar', '2018-09-16 13:00:00', 1, 'RSV1', '2:2', '0:0', NULL),
(280, 'Kreisklasse A Ost', 6, 'Bujendorfer Spvg', 'TSV Lütjenburg II', '2018-09-16 14:00:00', 1, 'Bujendorf1', '7:1', '1:0', NULL),
(281, 'Kreisklasse A Ost', 6, 'SV Heringsdorf', 'SG Scharbeutz/Süsel', '2018-09-16 15:00:00', 1, 'Hering1neu', '2:1', '2:0', NULL),
(282, 'Kreisklasse A Ost', 6, 'SV Großenbrode', 'TSV Malente II', '2018-09-16 15:00:00', 1, 'Großenbrode1 Stadion', '2:2', '1:1', NULL),
(283, 'Kreisklasse A Ost', 7, 'SG Insel Fehmarn II', 'SV Großenbrode', '2018-09-22 15:00:00', 1, 'RSV1', '3:0', '2:0', NULL),
(284, 'Kreisklasse A Ost', 7, 'TSV Malente II', 'SV Heringsdorf', '2018-09-22 15:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '0:3', '0:2', NULL),
(285, 'Kreisklasse A Ost', 7, 'TSV Lütjenburg II', 'TSV Lepahn', '2018-09-22 16:00:00', 1, 'Rasenplatz Lütjenburg	', '0:3', '0:2', NULL),
(286, 'Kreisklasse A Ost', 7, 'SG Lensahn/Kabelhorst I', 'TSV Wentorf', '2018-09-23 12:45:00', 1, 'Lensahn Kunstrasen', '3:4', '2:2', NULL),
(287, 'Kreisklasse A Ost', 7, 'SC Cismar', 'TSV Schönwalde', '2018-09-23 15:00:00', 1, 'Gildeplatz', '1:5', '1.1', NULL),
(288, 'Kreisklasse A Ost', 7, 'SG Baltic 1', 'TSV Selent', '2018-09-23 15:00:00', 1, 'Dahme1', '1:7', '0:2', NULL),
(289, 'Kreisklasse A Ost', 7, 'SG Scharbeutz/Süsel', 'Bujendorfer Spvg', '2018-09-23 15:00:00', 1, 'GleschendorfB-Kunstrasen', '0:5', '0:2', NULL),
(290, 'Kreisklasse A Ost', 8, 'TSV Selent', 'TSV Lütjenburg II', '2018-09-28 19:00:00', 1, 'OHLA-Stadion', '4:1', '4:1', NULL),
(291, 'Kreisklasse A Ost', 8, 'TSV Wentorf', 'SG Baltic 1', '2018-09-29 15:30:00', 1, 'Sportplatz Wentorf', '11:0', '4:0', NULL),
(292, 'Kreisklasse A Ost', 8, 'TSV Lepahn', 'SG Scharbeutz/Süsel', '2018-09-29 17:00:00', 1, 'A-Platz Lepahn', '2:1', '0:0', NULL),
(293, 'Kreisklasse A Ost', 8, 'Bujendorfer Spvg', 'TSV Malente II', '2018-09-30 14:00:00', 1, 'Bujendorf1', '14:1', '4:0', NULL),
(294, 'Kreisklasse A Ost', 8, 'SV Heringsdorf', 'SG Insel Fehmarn II', '2018-09-30 15:00:00', 1, 'Hering1neu', '1:1', '0:0', NULL),
(295, 'Kreisklasse A Ost', 8, 'SV Großenbrode', 'SC Cismar', '2018-09-30 15:00:00', 1, 'Großenbrode1 Stadion', '5:1', '1:0', NULL),
(296, 'Kreisklasse A Ost', 8, 'TSV Schönwalde', 'SG Lensahn/Kabelhorst I', '2018-09-30 15:00:00', 1, 'Schönwalde1', '4:4', '1:1', NULL),
(297, 'Kreisklasse A Ost', 9, 'TSV Malente II', 'TSV Lepahn', '2018-10-06 15:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '0:0', '0:0', NULL),
(298, 'Kreisklasse A Ost', 9, 'TSV Lütjenburg II', 'TSV Wentorf', '2018-10-06 16:00:00', 1, 'Rasenplatz Lütjenburg	', '2:2', '1:0', NULL),
(299, 'Kreisklasse A Ost', 9, 'SG Insel Fehmarn II', 'Bujendorfer Spvg', '2018-10-07 13:00:00', 1, 'RSV1', '2:3', '1:1', NULL),
(300, 'Kreisklasse A Ost', 9, 'SC Cismar', 'SG Lensahn/Kabelhorst I', '2018-10-07 15:00:00', 1, 'Gildeplatz', '4:1', '2:1', NULL),
(301, 'Kreisklasse A Ost', 9, 'SG Baltic 1', 'TSV Schönwalde', '2018-10-07 15:00:00', 1, 'Dahme1', '1:1', '0:1', NULL),
(302, 'Kreisklasse A Ost', 9, 'SV Großenbrode', 'SV Heringsdorf', '2018-10-07 15:00:00', 1, 'Großenbrode1 Stadion', '3:0', '0:0', NULL),
(303, 'Kreisklasse A Ost', 9, 'SG Scharbeutz/Süsel', 'TSV Selent', '2018-10-07 15:00:00', 1, 'GleschendorfB-Kunstrasen', '3:3', '3:2', NULL),
(304, 'Kreisklasse A Ost', 10, 'TSV Selent', 'TSV Malente II', '2018-10-12 19:00:00', 1, 'OHLA-Stadion', '3:2', '2:2', NULL),
(305, 'Kreisklasse A Ost', 10, 'TSV Wentorf', 'SG Scharbeutz/Süsel', '2018-10-13 15:30:00', 1, 'Sportplatz Wentorf', '0:2', '0:1', NULL),
(306, 'Kreisklasse A Ost', 10, 'TSV Lepahn', 'SG Insel Fehmarn II', '2018-10-13 17:00:00', 1, 'A-Platz Lepahn', '2:2', '1:1', NULL),
(307, 'Kreisklasse A Ost', 10, 'SG Lensahn/Kabelhorst I', 'SG Baltic 1', '2018-10-14 12:45:00', 1, 'Kabel1', '2:0', '1:0', NULL),
(308, 'Kreisklasse A Ost', 10, 'Bujendorfer Spvg', 'SV Großenbrode', '2018-10-14 14:00:00', 1, 'Bujendorf1', '8:0', '2:0', NULL),
(309, 'Kreisklasse A Ost', 10, 'SV Heringsdorf', 'SC Cismar', '2018-10-14 15:00:00', 1, 'Hering1neu', '2:5', '2:3', NULL),
(310, 'Kreisklasse A Ost', 10, 'TSV Schönwalde', 'TSV Lütjenburg II', '2018-10-14 15:00:00', 1, 'Schönwalde1', '3:0', '2:0', NULL),
(311, 'Kreisklasse A Ost', 11, 'TSV Malente II', 'TSV Wentorf', '2018-10-20 15:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '0:4', '0:3', NULL),
(312, 'Kreisklasse A Ost', 11, 'TSV Lütjenburg II', 'SG Lensahn/Kabelhorst I', '2018-10-20 16:00:00', 1, 'Rasenplatz Lütjenburg	', '3:2', '1:0', NULL),
(313, 'Kreisklasse A Ost', 11, 'SG Insel Fehmarn II', 'TSV Selent', '2018-10-21 12:00:00', 1, 'RSV1', '1:3', '1:2', NULL),
(314, 'Kreisklasse A Ost', 11, 'SC Cismar', 'SG Baltic 1', '2018-10-21 15:00:00', 1, 'Gildeplatz', '5:1', '1:1', NULL),
(315, 'Kreisklasse A Ost', 11, 'SV Heringsdorf', 'Bujendorfer Spvg', '2018-10-21 15:00:00', 1, 'Hering1neu', '2:0', '0:0', NULL),
(316, 'Kreisklasse A Ost', 11, 'SV Großenbrode', 'TSV Lepahn', '2018-10-21 15:00:00', 1, 'Großenbrode1 Stadion', '3:1', '1:0', NULL),
(317, 'Kreisklasse A Ost', 11, 'SG Scharbeutz/Süsel', 'TSV Schönwalde', '2018-10-21 15:00:00', 1, 'GleschendorfB-Kunstrasen', '5:5', '2:4', NULL),
(318, 'Kreisklasse A Ost', 12, 'SV Großenbrode', 'TSV Selent', '2018-10-27 14:00:00', 1, 'Großenbrode1 Stadion', '2:3', '0:1', NULL),
(319, 'Kreisklasse A Ost', 12, 'TSV Wentorf', 'SG Insel Fehmarn II', '2018-10-27 15:30:00', 1, 'Sportplatz Wentorf', '2:2', '2:1', NULL),
(320, 'Kreisklasse A Ost', 12, 'TSV Lepahn', 'SV Heringsdorf', '2018-10-27 17:00:00', 1, 'A-Platz Lepahn', '0:2', '0:0', NULL),
(321, 'Kreisklasse A Ost', 12, 'SG Lensahn/Kabelhorst I', 'SG Scharbeutz/Süsel', '2018-10-28 11:45:00', 1, 'Lensahn Kunstrasen', '0:3', '0:0', NULL),
(322, 'Kreisklasse A Ost', 12, 'Bujendorfer Spvg', 'SC Cismar', '2018-10-28 14:00:00', 1, 'Bujendorf1', '1:2', '1:0', NULL),
(323, 'Kreisklasse A Ost', 12, 'SG Baltic 1', 'TSV Lütjenburg II', '2018-10-28 15:00:00', 1, 'Dahme1', '0:3', '0:2', NULL),
(324, 'Kreisklasse A Ost', 12, 'TSV Schönwalde', 'TSV Malente II', '2018-10-28 15:00:00', 1, 'Schönwalde1', '7:2', '1:1', NULL),
(325, 'Kreisklasse A Ost', 13, 'TSV Malente II', 'SG Lensahn/Kabelhorst I', '2018-11-03 15:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '2:2', '1:1', NULL),
(326, 'Kreisklasse A Ost', 13, 'SG Insel Fehmarn II', 'TSV Schönwalde', '2018-11-04 12:00:00', 1, 'RSV1', '2:1', '1:0', NULL),
(327, 'Kreisklasse A Ost', 13, 'Bujendorfer Spvg', 'TSV Lepahn', '2018-11-04 14:00:00', 1, 'Bujendorf1', '4:1', '3:1', NULL),
(328, 'Kreisklasse A Ost', 13, 'SV Großenbrode', 'TSV Wentorf', '2018-11-04 14:00:00', 1, 'Großenbrode1 Stadion', '2:4', '2:2', NULL),
(329, 'Kreisklasse A Ost', 13, 'SC Cismar', 'TSV Lütjenburg II', '2018-11-04 15:00:00', 1, 'Gildeplatz', '2:1', '0:0', NULL),
(330, 'Kreisklasse A Ost', 13, 'SV Heringsdorf', 'TSV Selent', '2018-11-04 15:00:00', 1, 'Hering1neu', '1:4', '0:2', NULL),
(331, 'Kreisklasse A Ost', 13, 'SG Scharbeutz/Süsel', 'SG Baltic 1', '2018-11-04 15:00:00', 1, 'GleschendorfB-Kunstrasen', '2:1', '2:1', NULL),
(332, 'Kreisklasse A Ost', 14, 'TSV Malente II', 'TSV Lütjenburg II', '2018-11-10 14:00:00', 1, 'Stadion Ernst-Ruediger-Sportzentrum', '2:1', '0:0', NULL),
(333, 'Kreisklasse A Ost', 14, 'TSV Lepahn', 'TSV Selent', '2018-11-10 17:00:00', 1, 'A-Platz Lepahn', '0:2', '0:0', NULL),
(334, 'Kreisklasse A Ost', 14, 'SG Lensahn/Kabelhorst I', 'SV Großenbrode', '2018-11-11 11:45:00', 1, 'Lensahn Kunstrasen', '3:1', '2:0', NULL),
(335, 'Kreisklasse A Ost', 14, 'SG Insel Fehmarn II', 'SG Baltic 1', '2018-11-11 12:00:00', 1, 'RSV1', '0:0', '0:0', NULL),
(336, 'Kreisklasse A Ost', 14, 'Bujendorfer Spvg', 'TSV Wentorf', '2018-11-11 14:00:00', 1, 'Bujendorf1', '3:1', '2:0', NULL),
(337, 'Kreisklasse A Ost', 14, 'SV Heringsdorf', 'TSV Schönwalde', '2018-11-11 15:00:00', 1, 'Hering1neu', '5:0', '3:0', NULL),
(338, 'Kreisklasse A Ost', 14, 'SG Scharbeutz/Süsel', 'SC Cismar', '2018-11-11 15:00:00', 1, 'GleschendorfB-Kunstrasen', '3:1', '1:1', NULL),
(339, 'Kreisklasse A Ost', 15, 'TSV Selent', 'Bujendorfer Spvg', '2018-11-16 19:00:00', 1, 'OHLA-Stadion', '1:2', '0:0', NULL),
(340, 'Kreisklasse A Ost', 15, 'TSV Lütjenburg II', 'SG Scharbeutz/Süsel', '2018-11-17 14:00:00', 1, 'Rasenplatz Lütjenburg	', '2:1', '2:0', NULL),
(341, 'Kreisklasse A Ost', 15, 'TSV Wentorf', 'SV Heringsdorf', '2018-11-17 14:00:00', 1, 'Sportplatz Wentorf', '2:3', '2:2', NULL),
(342, 'Kreisklasse A Ost', 15, 'SG Lensahn/Kabelhorst I', 'SG Insel Fehmarn II', '2018-11-18 11:45:00', 1, 'Kabel1', '0:2', '0:1', NULL),
(343, 'Kreisklasse A Ost', 15, 'SC Cismar', 'TSV Lepahn', '2018-11-18 14:00:00', 1, 'Gildeplatz', '8:2', '3:2', NULL),
(344, 'Kreisklasse A Ost', 15, 'TSV Schönwalde', 'SV Großenbrode', '2018-11-18 14:00:00', 1, 'Schönwalde1', '2:1', '2:0', NULL),
(345, 'Kreisklasse A Ost', 15, 'SG Baltic 1', 'TSV Malente II', '2018-11-18 15:00:00', 1, 'Dahme1', '0:1', '0:0', NULL),
(346, 'Kreisklasse A Ost', 16, 'TSV Wentorf', 'TSV Selent', '2019-03-09 15:30:00', 0, 'Sportplatz Wentorf', NULL, NULL, NULL),
(347, 'Kreisklasse A Ost', 16, 'TSV Lütjenburg II', 'SV Großenbrode', '2019-03-09 16:00:00', 0, 'Rasenplatz Lütjenburg	', NULL, NULL, NULL),
(348, 'Kreisklasse A Ost', 16, 'SG Lensahn/Kabelhorst I', 'Bujendorfer Spvg', '2019-03-10 12:45:00', 0, 'Lensahn Kunstrasen', NULL, NULL, NULL),
(349, 'Kreisklasse A Ost', 16, 'TSV Schönwalde', 'TSV Lepahn', '2019-03-10 15:00:00', 0, 'Schönwalde1', NULL, NULL, NULL),
(350, 'Kreisklasse A Ost', 16, 'SG Baltic 1', 'SV Heringsdorf', '2019-03-10 15:00:00', 0, 'Dahme1', NULL, NULL, NULL),
(351, 'Kreisklasse A Ost', 16, 'SG Scharbeutz/Süsel', 'SG Insel Fehmarn II', '2019-03-10 15:00:00', 0, 'GleschendorfB-Kunstrasen', NULL, NULL, NULL),
(352, 'Kreisklasse A Ost', 16, 'SC Cismar', 'TSV Malente II', '2019-03-10 15:00:00', 0, 'Gildeplatz', NULL, NULL, NULL),
(353, 'Kreisklasse A Ost', 17, 'TSV Selent', 'TSV Schönwalde', '2019-03-15 19:00:00', 0, 'OHLA-Stadion', NULL, NULL, NULL),
(354, 'Kreisklasse A Ost', 17, 'TSV Wentorf', 'SC Cismar', '2019-03-16 15:30:00', 0, 'Sportplatz Wentorf', NULL, NULL, NULL),
(355, 'Kreisklasse A Ost', 17, 'TSV Lepahn', 'SG Lensahn/Kabelhorst I', '2019-03-16 17:00:00', 0, 'A-Platz Lepahn', NULL, NULL, NULL),
(356, 'Kreisklasse A Ost', 17, 'SG Insel Fehmarn II', 'TSV Malente II', '2019-03-17 13:00:00', 0, 'RSV1', NULL, NULL, NULL),
(357, 'Kreisklasse A Ost', 17, 'Bujendorfer Spvg', 'SG Baltic 1', '2019-03-17 14:00:00', 0, 'Bujendorf1', NULL, NULL, NULL),
(358, 'Kreisklasse A Ost', 17, 'SV Heringsdorf', 'TSV Lütjenburg II', '2019-03-17 15:00:00', 0, 'Hering1neu', NULL, NULL, NULL),
(359, 'Kreisklasse A Ost', 17, 'SV Großenbrode', 'SG Scharbeutz/Süsel', '2019-03-17 15:00:00', 0, 'Großenbrode1 Stadion', NULL, NULL, NULL),
(360, 'Kreisklasse A Ost', 18, 'TSV Malente II', 'SV Großenbrode', '2019-03-23 15:00:00', 0, 'Stadion Ernst-Ruediger-Sportzentrum', NULL, NULL, NULL),
(361, 'Kreisklasse A Ost', 18, 'TSV Lütjenburg II', 'Bujendorfer Spvg', '2019-03-23 16:00:00', 0, 'Rasenplatz Lütjenburg	', NULL, NULL, NULL),
(362, 'Kreisklasse A Ost', 18, 'SG Lensahn/Kabelhorst I', 'TSV Selent', '2019-03-24 12:45:00', 0, 'Kabel1', NULL, NULL, NULL),
(363, 'Kreisklasse A Ost', 18, 'SG Baltic 1', 'TSV Lepahn', '2019-03-24 15:00:00', 0, 'Dahme1', NULL, NULL, NULL),
(364, 'Kreisklasse A Ost', 18, 'SG Scharbeutz/Süsel', 'SV Heringsdorf', '2019-03-24 15:00:00', 0, 'GleschendorfB-Kunstrasen', NULL, NULL, NULL),
(365, 'Kreisklasse A Ost', 18, 'SC Cismar', 'SG Insel Fehmarn II', '2019-03-24 15:00:00', 0, 'Gildeplatz', NULL, NULL, NULL),
(366, 'Kreisklasse A Ost', 18, 'TSV Schönwalde', 'TSV Wentorf', '2019-03-24 15:00:00', 0, 'Schönwalde1', NULL, NULL, NULL),
(367, 'Kreisklasse A Ost', 19, 'TSV Selent', 'SG Baltic 1', '2019-03-29 19:00:00', 0, 'OHLA-Stadion', NULL, NULL, NULL),
(368, 'Kreisklasse A Ost', 19, 'TSV Wentorf', 'SG Lensahn/Kabelhorst I', '2019-03-30 15:30:00', 0, 'Sportplatz Wentorf', NULL, NULL, NULL),
(369, 'Kreisklasse A Ost', 19, 'TSV Lepahn', 'TSV Lütjenburg II', '2019-03-30 17:00:00', 0, 'A-Platz Lepahn', NULL, NULL, NULL),
(370, 'Kreisklasse A Ost', 19, 'Bujendorfer Spvg', 'SG Scharbeutz/Süsel', '2019-03-31 14:00:00', 0, 'Bujendorf1', NULL, NULL, NULL),
(371, 'Kreisklasse A Ost', 19, 'TSV Schönwalde', 'SC Cismar', '2019-03-31 15:00:00', 0, 'Schönwalde1', NULL, NULL, NULL),
(372, 'Kreisklasse A Ost', 19, 'SV Großenbrode', 'SG Insel Fehmarn II', '2019-03-31 15:00:00', 0, 'Großenbrode1 Stadion', NULL, NULL, NULL),
(373, 'Kreisklasse A Ost', 19, 'SV Heringsdorf', 'TSV Malente II', '2019-03-31 15:00:00', 0, 'Hering1neu', NULL, NULL, NULL),
(374, 'Kreisklasse A Ost', 20, 'TSV Malente II', 'Bujendorfer Spvg', '2019-04-06 15:00:00', 0, 'Stadion Ernst-Ruediger-Sportzentrum', NULL, NULL, NULL),
(375, 'Kreisklasse A Ost', 20, 'TSV Lütjenburg II', 'TSV Selent', '2019-04-06 16:00:00', 0, 'Rasenplatz Lütjenburg	', NULL, NULL, NULL),
(376, 'Kreisklasse A Ost', 20, 'SG Lensahn/Kabelhorst I', 'TSV Schönwalde', '2019-04-07 12:45:00', 0, 'Lensahn Kunstrasen', NULL, NULL, NULL),
(377, 'Kreisklasse A Ost', 20, 'SG Insel Fehmarn II', 'SV Heringsdorf', '2019-04-07 13:00:00', 0, 'RSV1', NULL, NULL, NULL);
INSERT INTO `spiele` (`spiel_id`, `liga`, `spieltag`, `heimmannschaft`, `auswaertsmannschaft`, `anstosszeit`, `spielvorbei`, `spielort`, `endergebnis`, `halbzeitergebnis`, `spieldetails`) VALUES
(378, 'Kreisklasse A Ost', 20, 'SG Scharbeutz/Süsel', 'TSV Lepahn', '2019-04-07 15:00:00', 0, 'GleschendorfB-Kunstrasen', NULL, NULL, NULL),
(379, 'Kreisklasse A Ost', 20, 'SC Cismar', 'SV Großenbrode', '2019-04-07 15:00:00', 0, 'Gildeplatz', NULL, NULL, NULL),
(380, 'Kreisklasse A Ost', 20, 'SG Baltic 1', 'TSV Wentorf', '2019-04-07 15:00:00', 0, 'Dahme1', NULL, NULL, NULL),
(381, 'Kreisklasse A Ost', 21, 'TSV Selent', 'SG Scharbeutz/Süsel', '2019-04-12 19:00:00', 0, 'OHLA-Stadion', NULL, NULL, NULL),
(382, 'Kreisklasse A Ost', 21, 'TSV Wentorf', 'TSV Lütjenburg II', '2019-04-13 15:30:00', 0, 'Sportplatz Wentorf', NULL, NULL, NULL),
(383, 'Kreisklasse A Ost', 21, 'TSV Lepahn', 'TSV Malente II', '2019-04-13 17:00:00', 0, 'A-Platz Lepahn', NULL, NULL, NULL),
(384, 'Kreisklasse A Ost', 21, 'SG Lensahn/Kabelhorst I', 'SC Cismar', '2019-04-14 12:45:00', 0, 'Kabel1', NULL, NULL, NULL),
(385, 'Kreisklasse A Ost', 21, 'Bujendorfer Spvg', 'SG Insel Fehmarn II', '2019-04-14 14:00:00', 0, 'Bujendorf1', NULL, NULL, NULL),
(386, 'Kreisklasse A Ost', 21, 'TSV Schönwalde', 'SG Baltic 1', '2019-04-14 15:00:00', 0, 'Schönwalde1', NULL, NULL, NULL),
(387, 'Kreisklasse A Ost', 21, 'SV Heringsdorf', 'SV Großenbrode', '2019-04-14 15:00:00', 0, 'Hering1neu', NULL, NULL, NULL),
(388, 'Kreisklasse A Ost', 22, 'TSV Malente II', 'TSV Selent', '2019-04-27 15:00:00', 0, 'Stadion Ernst-Ruediger-Sportzentrum', NULL, NULL, NULL),
(389, 'Kreisklasse A Ost', 22, 'TSV Lütjenburg II', 'TSV Schönwalde', '2019-04-27 16:00:00', 0, 'Rasenplatz Lütjenburg	', NULL, NULL, NULL),
(390, 'Kreisklasse A Ost', 22, 'SG Insel Fehmarn II', 'TSV Lepahn', '2019-04-28 13:00:00', 0, 'RSV1', NULL, NULL, NULL),
(391, 'Kreisklasse A Ost', 22, 'SV Großenbrode', 'Bujendorfer Spvg', '2019-04-28 15:00:00', 0, 'Großenbrode1 Stadion', NULL, NULL, NULL),
(392, 'Kreisklasse A Ost', 22, 'SC Cismar', 'SV Heringsdorf', '2019-04-28 15:00:00', 0, 'Gildeplatz', NULL, NULL, NULL),
(393, 'Kreisklasse A Ost', 22, 'SG Baltic 1', 'SG Lensahn/Kabelhorst I', '2019-04-28 15:00:00', 0, 'Dahme1', NULL, NULL, NULL),
(394, 'Kreisklasse A Ost', 22, 'SG Scharbeutz/Süsel', 'TSV Wentorf', '2019-04-28 16:00:00', 0, 'GleschendorfB-Kunstrasen', NULL, NULL, NULL),
(395, 'Kreisklasse A Ost', 23, 'TSV Selent', 'SG Insel Fehmarn II', '2019-05-03 19:00:00', 0, 'OHLA-Stadion', NULL, NULL, NULL),
(396, 'Kreisklasse A Ost', 23, 'TSV Wentorf', 'TSV Malente II', '2019-05-04 15:30:00', 0, 'Sportplatz Wentorf', NULL, NULL, NULL),
(397, 'Kreisklasse A Ost', 23, 'TSV Lepahn', 'SV Großenbrode', '2019-05-04 17:00:00', 0, 'A-Platz Lepahn', NULL, NULL, NULL),
(398, 'Kreisklasse A Ost', 23, 'SG Lensahn/Kabelhorst I', 'TSV Lütjenburg II', '2019-05-05 12:45:00', 0, 'Lensahn Kunstrasen', NULL, NULL, NULL),
(399, 'Kreisklasse A Ost', 23, 'Bujendorfer Spvg', 'SV Heringsdorf', '2019-05-05 14:00:00', 0, 'Bujendorf1', NULL, NULL, NULL),
(400, 'Kreisklasse A Ost', 23, 'SG Baltic 1', 'SC Cismar', '2019-05-05 15:00:00', 0, 'Dahme1', NULL, NULL, NULL),
(401, 'Kreisklasse A Ost', 23, 'TSV Schönwalde', 'SG Scharbeutz/Süsel', '2019-05-05 15:00:00', 0, 'Schönwalde1', NULL, NULL, NULL),
(402, 'Kreisklasse A Ost', 24, 'TSV Malente II', 'TSV Schönwalde', '2019-05-11 15:00:00', 0, 'Stadion Ernst-Ruediger-Sportzentrum', NULL, NULL, NULL),
(403, 'Kreisklasse A Ost', 24, 'TSV Lütjenburg II', 'SG Baltic 1', '2019-05-11 16:00:00', 0, 'Rasenplatz Lütjenburg	', NULL, NULL, NULL),
(404, 'Kreisklasse A Ost', 24, 'SG Insel Fehmarn II', 'TSV Wentorf', '2019-05-12 13:00:00', 0, 'RSV1', NULL, NULL, NULL),
(405, 'Kreisklasse A Ost', 24, 'SV Heringsdorf', 'TSV Lepahn', '2019-05-12 15:00:00', 0, 'Hering1neu', NULL, NULL, NULL),
(406, 'Kreisklasse A Ost', 24, 'SC Cismar', 'Bujendorfer Spvg', '2019-05-12 15:00:00', 0, 'Gildeplatz', NULL, NULL, NULL),
(407, 'Kreisklasse A Ost', 24, 'SG Scharbeutz/Süsel', 'SG Lensahn/Kabelhorst I', '2019-05-12 15:00:00', 0, 'GleschendorfB-Kunstrasen', NULL, NULL, NULL),
(408, 'Kreisklasse A Ost', 24, 'SV Großenbrode', 'TSV Selent', '2019-05-12 15:00:00', 0, 'Großenbrode1 Stadion', NULL, NULL, NULL),
(409, 'Kreisklasse A Ost', 25, 'TSV Selent', 'SV Heringsdorf', '2019-05-17 19:00:00', 0, 'OHLA-Stadion', NULL, NULL, NULL),
(410, 'Kreisklasse A Ost', 25, 'TSV Wentorf', 'SV Großenbrode', '2019-05-18 15:30:00', 0, 'Sportplatz Wentorf', NULL, NULL, NULL),
(411, 'Kreisklasse A Ost', 25, 'TSV Lütjenburg II', 'SC Cismar', '2019-05-18 16:00:00', 0, 'Rasenplatz Lütjenburg	', NULL, NULL, NULL),
(412, 'Kreisklasse A Ost', 25, 'TSV Lepahn', 'Bujendorfer Spvg', '2019-05-18 17:00:00', 0, 'A-Platz Lepahn', NULL, NULL, NULL),
(413, 'Kreisklasse A Ost', 25, 'SG Lensahn/Kabelhorst I', 'TSV Malente II', '2019-05-19 12:45:00', 0, 'Kabel1', NULL, NULL, NULL),
(414, 'Kreisklasse A Ost', 25, 'TSV Schönwalde', 'SG Insel Fehmarn II', '2019-05-19 15:00:00', 0, 'Schönwalde1', NULL, NULL, NULL),
(415, 'Kreisklasse A Ost', 25, 'SG Baltic 1', 'SG Scharbeutz/Süsel', '2019-05-19 15:00:00', 0, 'Dahme1', NULL, NULL, NULL),
(416, 'Kreisklasse A Ost', 26, 'TSV Selent', 'SC Cismar', '2019-05-24 19:00:00', 0, 'OHLA-Stadion', NULL, NULL, NULL),
(417, 'Kreisklasse A Ost', 26, 'SV Großenbrode', 'SG Baltic 1', '2019-05-25 15:00:00', 0, 'Großenbrode1 Stadion', NULL, NULL, NULL),
(418, 'Kreisklasse A Ost', 26, 'TSV Malente II', 'SG Scharbeutz/Süsel', '2019-05-25 15:00:00', 0, 'Stadion Ernst-Ruediger-Sportzentrum', NULL, NULL, NULL),
(419, 'Kreisklasse A Ost', 26, 'SG Insel Fehmarn I', 'TSV Lütjenburg II', '2019-05-25 17:00:00', 0, 'RSV1', NULL, NULL, NULL),
(420, 'Kreisklasse A Ost', 26, 'TSV Lepahn', 'TSV Wentorf', '2019-05-25 17:00:00', 0, 'A-Platz Lepahn', NULL, NULL, NULL),
(421, 'Kreisklasse A Ost', 26, 'Bujendorfer Spvg', 'TSV Schönwalde', '2019-05-26 14:00:00', 0, 'Bujendorf1', NULL, NULL, NULL),
(422, 'Kreisklasse A Ost', 26, 'SV Heringsdorf', 'SG Lensahn/Kabelhorst I', '2019-05-26 15:00:00', 0, 'Hering1neu', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `spielorte`
--

CREATE TABLE `spielorte` (
  `platzname` varchar(45) CHARACTER SET utf8 NOT NULL,
  `mannschaft` varchar(45) CHARACTER SET utf8 NOT NULL,
  `adresse` varchar(45) CHARACTER SET utf8 NOT NULL,
  `breitengr_max` double NOT NULL,
  `laengengr_max` double NOT NULL,
  `breitengr_min` double NOT NULL,
  `laengengr_min` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `spielorte`
--

INSERT INTO `spielorte` (`platzname`, `mannschaft`, `adresse`, `breitengr_max`, `laengengr_max`, `breitengr_min`, `laengengr_min`) VALUES
('A-Platz Dersau', 'ASV Dersau', 'Am Sportplatz, 24326 Dersau', 54.109101, 10.34624, 54.108092, 10.344841),
('A-Platz Lepahn', 'TSV Lepahn', 'Zum Sportplatz, 24211 Lehmkuhlen', 54.219478, 10.383966, 54.218691, 10.381936),
('A-Platz Schinkel', '1. FC Schinkel', 'Haupstraße, 24214 Schinkel', 54.35869, 9.95446, 54.357454, 9.95155),
('A-Platz SVE Comet Kiel', 'SVE Comet Kiel', 'Passader Straße 16, 24148 Kiel', 54.324846, 10.191708, 54.323754, 10.18939),
('Bujendorf1', 'Bujendorfer Spvg', 'Dorfstraße, 23701 Süsel', 54.106525, 10.704098, 54.105691, 10.702423),
('Dahme1', 'SG Baltic', 'Waldstraße, 23747 Dahme', 54.215611, 11.081764, 54.214621, 11.080496),
('Ellerbek Platz A', 'SVE Comet Kiel', 'Radsredder 15, 24148 Kiel', 54.324189, 10.180979, 54.322705, 10.178438),
('Eutin Kunstrasen', 'Eutin 08', 'Steinredder, 23701 Eutin', 54.149768, 10.631, 54.14864, 10.629472),
('Fritz-Latendorf-Stadion ', 'Eutin 08', 'Steinredder, 23701 Eutin', 54.149474, 10.628318, 54.148107, 10.626393),
('Gildeplatz', 'SC Cismar', 'Gildestraße, 23743 Grömitz', 54.149469, 10.953568, 54.148175, 10.951668),
('GleschendorfB-Kunstrasen', 'SG Scharbeutz/Süsel', 'Fierthstraße, 23684 Scharbeutz', 54.035257, 10.663215, 54.03433, 10.662139),
('Großenbrode1 Stadion', 'SV Großenbrode', 'Am Sportplatz 1, 23775 Großenbrode	', 54.369114, 11.089944, 54.368098, 11.088595),
('Großenbrode2', 'SV Großenbrode', 'Am Sportplatz 1, 23775 Großenbrode', 54.367584, 11.090356, 54.366509, 11.08888),
('Hauptplatz', 'FC Kilia Kiel', 'Hasseldieksdammer Weg 165, 24114 Kiel', 54.319256, 10.101834, 54.318042, 10.09976),
('Hering1neu', 'SV Heringsdorf', 'Am Langen Acker, 23777 Heringsdorf', 54.29822, 11.004635, 54.296993, 11.002834),
('Kabel1', 'SG Lensahn/Kabelhorst', 'Grünbek, 23738 Kabelhorst', 54.223571, 10.93224, 54.222941, 10.931197),
('Lensahn Kunstrasen', 'SG Lensahn/Kabelhorst', 'Doktor-Julius-Stinde-Straße, 23738 Lensahn', 54.214799, 10.880357, 54.213967, 10.878494),
('OHLA-Stadion', 'TSV Selent', 'Am See, 24238 Lammershagen', 54.290395, 10.444766, 54.289025, 10.442903),
('Rasenplatz Lütjenburg	', 'TSV Lütjenburg', 'Kieler Straße 34, 24321 Luetjenburg', 54.294894, 10.575434, 54.293569, 10.572475),
('RSV1', 'SG Insel Fehmarn', 'Am Sportplatz, 23769 Fehmarn', 54.448533, 11.147656, 54.447319, 11.146191),
('Schiffsthal Stadion', 'TSV Plön', 'Am Schiffstahl 13, 24306 Plön', 54.166151, 10.416529, 54.164712, 10.414526),
('Schönwalde1', 'TSV Schönwalde', 'Jahnweg, 23744 Schoenwalde am Bungsberg', 54.185524, 10.759212, 54.184263, 10.756989),
('Seeweg Bornhoeved A-Platz', 'SG Bornhöved/Schmalensee', 'Seeweg, 24619 Bornhöved', 54.077063, 10.234444, 54.075798, 10.232879),
('Sportanlage Dänischenhagen	', 'MTV Dänischenhagen', 'Schulstraße 46, 24229 Dänischenhagen', 54.428604, 10.124796, 54.427031, 10.121815),
('Sportplatz Dieter von Bortstel	', 'Dobersdorfer SV', 'Am Sportplatz 2, 24232 Dobersdorf', 54.333885, 10.294593, 54.332807, 10.292447),
('Sportplatz Meimersdorf', 'SSG Rot-Schwarz Kiel', 'Kieler Weg 74, 24145 Kiel', 54.282295, 10.12105, 54.281432, 10.118921),
('Sportplatz Stein', 'TSV Stein', 'Am Sportplatz, 24235 Stein', 54.415686, 10.277999, 54.414647, 10.276361),
('Sportplatz Wentorf', 'TSV Wentorf', 'Schoolredder, 24321 Klamp', 54.28494, 10.550522, 54.28378, 10.549045),
('Sportzentrum Platz4', 'TSV Kronshagen', 'Suchsdorfer Weg 68, 24119 Kronshagen', 54.342614, 10.082873, 54.341287, 10.08115),
('Stadion Ernst-Ruediger-Sportzentrum', 'TSV Malente', 'Neversfelder Straße 11, 23714 Malente', 54.178483, 10.551487, 54.177115, 10.549817),
('Stadion Preetz', 'Preetzer TSV', 'Am Jahnplatz, 24211 Preetz', 54.22591, 10.272894, 54.224243, 10.270034),
('Stoschplatz Hauptplatz', 'VfR Laboe', 'Heikendorfer Weg 41, 24235 Laboe', 54.397651, 10.220495, 54.396148, 10.217984),
('zuhause', 'klaas', 'Am Südersoll ', 55.442332, 11.398905, 54.332016, 11.177389);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tickerereignisse`
--

CREATE TABLE `tickerereignisse` (
  `tickerereignis_id` int(11) NOT NULL,
  `spieldetails_id` int(11) NOT NULL,
  `spielminute` int(11) NOT NULL,
  `ereignis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tickerereignisse`
--

INSERT INTO `tickerereignisse` (`tickerereignis_id`, `spieldetails_id`, `spielminute`, `ereignis`) VALUES
(161, 1, 46, 'Tor für den TSV Kronshagen!'),
(162, 1, 67, 'Das 2:0 für Kronshagen'),
(163, 1, 89, 'Der Anschlusstreffer! Nur noch 1:2'),
(164, 1, 90, 'Spielende'),
(165, 241, 33, 'Das 1:0 durch Torben Plagmann'),
(166, 241, 45, 'Halbzeit'),
(167, 241, 52, 'Da ist das 2:0 für den TSV Malente'),
(168, 241, 62, 'Nur 10 Minuten später schon das 3:0'),
(169, 241, 72, 'Da ist das 3:1! Ist da noch was drin für die SG?'),
(170, 241, 86, 'Die Entscheidung das 4:1'),
(171, 241, 90, 'Spielende'),
(172, 423, 1, 'qqqq'),
(173, 423, 1, 'qwe'),
(174, 423, 1, 'qwe'),
(175, 423, 46, 'qwe'),
(176, 423, 46, 'qwe'),
(177, 423, 46, 'qwe'),
(178, 423, 46, 'Spielende');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `mannschaften`
--
ALTER TABLE `mannschaften`
  ADD PRIMARY KEY (`mannschaftsname`);

--
-- Indizes für die Tabelle `spieldetails`
--
ALTER TABLE `spieldetails`
  ADD PRIMARY KEY (`spieldetails_id`);

--
-- Indizes für die Tabelle `spiele`
--
ALTER TABLE `spiele`
  ADD PRIMARY KEY (`spiel_id`),
  ADD KEY `spielort` (`spielort`),
  ADD KEY `spiel_ibfk_2` (`spieldetails`),
  ADD KEY `heimmannschaft` (`heimmannschaft`),
  ADD KEY `auswaertsmannschaft` (`auswaertsmannschaft`);

--
-- Indizes für die Tabelle `spielorte`
--
ALTER TABLE `spielorte`
  ADD PRIMARY KEY (`platzname`);

--
-- Indizes für die Tabelle `tickerereignisse`
--
ALTER TABLE `tickerereignisse`
  ADD PRIMARY KEY (`tickerereignis_id`),
  ADD KEY `fk_spieldetails_id` (`spieldetails_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `spieldetails`
--
ALTER TABLE `spieldetails`
  MODIFY `spieldetails_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=424;

--
-- AUTO_INCREMENT für Tabelle `spiele`
--
ALTER TABLE `spiele`
  MODIFY `spiel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=423;

--
-- AUTO_INCREMENT für Tabelle `tickerereignisse`
--
ALTER TABLE `tickerereignisse`
  MODIFY `tickerereignis_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `spiele`
--
ALTER TABLE `spiele`
  ADD CONSTRAINT `spiele_ibfk_2` FOREIGN KEY (`spieldetails`) REFERENCES `spieldetails` (`spieldetails_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `spiele_ibfk_3` FOREIGN KEY (`heimmannschaft`) REFERENCES `mannschaften` (`mannschaftsname`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `spiele_ibfk_4` FOREIGN KEY (`auswaertsmannschaft`) REFERENCES `mannschaften` (`mannschaftsname`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `spiele_ibfk_5` FOREIGN KEY (`spielort`) REFERENCES `spielorte` (`platzname`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `tickerereignisse`
--
ALTER TABLE `tickerereignisse`
  ADD CONSTRAINT `fk_spieldetails_id` FOREIGN KEY (`spieldetails_id`) REFERENCES `spieldetails` (`spieldetails_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
