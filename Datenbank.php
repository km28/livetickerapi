<?php

    /**
     * Created by PhpStorm.
     * User: Klaas
     * Date: 07.11.2018
     * Time: 16:10
     */
    class Datenbank
    {
        private $host = 'localhost';
        private $db_name = 'fussball_liveticker';
        private $username = 'root';
        private $password = '';
        private $conn;

        public function connect() {
            $this->conn = null;

            try{
                $this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db_name,
                    $this->username, $this->password);
                /**$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRORMODE_EXCEPTION);**/
            } catch(PDOException $exception){
                echo 'Connection Error: ' . $exception->getMessage();
            }

            return $this->conn;
        }

    }